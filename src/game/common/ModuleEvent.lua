---
--- Created by hollowJ.
--- DateTime: 2018/1/14 17:55
---
local Event = require("common.events")
ModuleEvent = {}

function ModuleEvent.Dispatch(eventId, ...)
    LogHelper.Debug("eventId:"..eventId)
    Event.Brocast(tostring(eventId), ...)
end

function ModuleEvent.AddListenner(eventId, handler)
    Event.AddListener(tostring(eventId), handler)
end

function ModuleEvent.RemoveListener(eventId, handler)
    Event.RemoveListener(tostring(eventId), handler)
end

return ModuleEvent