---
--- Created by hollowJ.
--- DateTime: 2018/1/11 21:33
---
require("common.FairyGUI")
require("common.Util")
PlayerPrefs = UnityEngine.PlayerPrefs
GameObject = UnityEngine.GameObject
Input = UnityEngine.Input
Camera = UnityEngine.Camera
KeyCode=UnityEngine.KeyCode
UIModule = KEngine.UI.UIModule
Log = KEngine.Log
--WindowInfo = HollowJ.Window.WindowInfo
AssetBundleLoader = KEngine.AssetBundleLoader
AudioLoader = KEngine.AudioLoader
Ease = DG.Tweening.Ease
DOTween = DG.Tweening.DOTween
TypeDictionary = FrameWork.Plugins.HollowJ.common.TypeDictionary
LuaHelper = FrameWork.Plugins.HollowJ.common.LuaHelper
print = LogHelper.Info
---@type UnityEngine.Physics
Physics=UnityEngine.Physics
---@type UnityEngine.LayerMask
LayerMask=UnityEngine.LayerMask
---@type UnityEngine.EventSystems.EventSystem
EventSystem=UnityEngine.EventSystems.EventSystem
--require("common.U3DConst")
--require("common.Util")
--require("common.func")
--require("common.init")
--require("common.CommonHelper")
