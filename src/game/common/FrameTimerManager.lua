---
--- Created by hollowJ.
--- DateTime: 2017/12/2 17:32
---
FrameTimerManager = {}
local registeredUpdateFunctions = {}
local registeredLateUpdateFunctions = {}
function FrameTimerManager.RegisterUpdate(func)
    registeredUpdateFunctions[func] = func
end
function FrameTimerManager.RemoveUpdate(func)
    registeredUpdateFunctions[func] = nil
end
function FrameTimerManager.RegisterLateUpdate(func)
    registeredLateUpdateFunctions[func] = func
end
function FrameTimerManager.RemoveLateUpdate(func)
    registeredLateUpdateFunctions[func] = nil
end
function FrameTimerManager.onUpdate()
    for _, func in pairs(registeredUpdateFunctions) do
        xpcall(func,function (err)
            LogHelper.Error(err)
        end)
    end
end
function FrameTimerManager.onLateUpdate()
    for _, func in pairs(registeredLateUpdateFunctions) do
        xpcall(func,function (err)
            LogHelper.Error(err)
        end)
    end
end
UpdateBeat:Add(FrameTimerManager.onUpdate)
LateUpdateBeat:Add(FrameTimerManager.onLateUpdate)