---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by hollowJ.
--- DateTime: 2018/6/19 21:03
---
require("usrdata.SaveModel")
BaseContainer = {
    class = "BaseContainer",
    data = {},
}
---@class BaseContainer :SaveModel
BaseContainer = SaveModel:New(BaseContainer)

function BaseContainer:Create(key)
    local instance = self:New()
    instance.key = key
    instance.items = {}
    return instance
end
function BaseContainer:GetAllItems()
    return self.items
end
function BaseContainer:AddItem(item)
    item.id = #self.items + 1
    table.insert(self.items, item)
    return true
end
function BaseContainer:Clear()
    self:OnLoad(nil)
end