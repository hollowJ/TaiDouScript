--主入口函数。从这里开始lua逻辑
--require("common.Import")
--require("common.ModuleRegister")
Main = {}
function Main.Main()
    print("logic start")

    ModuleEvent.Dispatch(ModuleConst.LOAD_STAGE_START, StageConst.Choose)
end

--场景切换通知
function OnLevelWasLoaded(level)
    collectgarbage("collect")
    Time.timeSinceLevelLoad = 0
end
function Main.LoadAllScript(allLuaPaths)
    local allModule = {}
    allLuaPaths:ForEach(function(path)
        print('require: ' .. path)
        local status,module = pcall(require,path)
        if status==true and type(module)=="table" then
            table.insert(allModule, module)
        else
            if status==false then
                LogHelper.Error('require error:: ' .. module)
                return
            end
        end
    end)
    for _, module in ipairs(allModule) do
        if module and module.OnFirstImport then
            module.OnFirstImport()
        end
    end
end
return Main