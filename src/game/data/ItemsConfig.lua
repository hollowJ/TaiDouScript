---auto generate begin---
ItemsConfig = {}
ItemsConfig.data = {
  {
    id = 1,
    name = "青铜手镯",
    icon = "equip_1",
    type = 2,
    refId = 1,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜手镯介绍",
    pileCount = 1,
    sellReward = {[5001]=1}
  },
  {
    id = 2,
    name = "完好的翅膀",
    icon = "equip_2",
    type = 2,
    refId = 2,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "完好的翅膀介绍",
    pileCount = 1,
    sellReward = {[5001]=2}
  },
  {
    id = 3,
    name = "青铜戒指",
    icon = "equip_3",
    type = 2,
    refId = 3,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜戒指介绍",
    pileCount = 1,
    sellReward = {[5001]=3}
  },
  {
    id = 4,
    name = "青铜盔甲",
    icon = "equip_4",
    type = 2,
    refId = 4,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜盔甲介绍",
    pileCount = 1,
    sellReward = {[5001]=4}
  },
  {
    id = 5,
    name = "青铜帽子",
    icon = "equip_5",
    type = 2,
    refId = 5,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜帽子介绍",
    pileCount = 1,
    sellReward = {[5001]=5}
  },
  {
    id = 6,
    name = "青铜斧头",
    icon = "equip_6",
    type = 2,
    refId = 6,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜斧头介绍",
    pileCount = 1,
    sellReward = {[5001]=6}
  },
  {
    id = 7,
    name = "青铜项链",
    icon = "equip_7",
    type = 2,
    refId = 7,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜项链介绍",
    pileCount = 1,
    sellReward = {[5001]=7}
  },
  {
    id = 8,
    name = "青铜鞋子",
    icon = "equip_8",
    type = 2,
    refId = 8,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "青铜鞋子介绍",
    pileCount = 1,
    sellReward = {[5001]=8}
  },
  {
    id = 9,
    name = "紫金手镯",
    icon = "equip_9",
    type = 2,
    refId = 9,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金手镯介绍",
    pileCount = 1,
    sellReward = {[5001]=9}
  },
  {
    id = 10,
    name = "紫翼翅膀",
    icon = "equip_10",
    type = 2,
    refId = 10,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫翼翅膀介绍",
    pileCount = 1,
    sellReward = {[5001]=10}
  },
  {
    id = 11,
    name = "紫金戒指",
    icon = "equip_11",
    type = 2,
    refId = 11,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金戒指介绍",
    pileCount = 1,
    sellReward = {[5001]=11}
  },
  {
    id = 12,
    name = "紫金衣",
    icon = "equip_12",
    type = 2,
    refId = 12,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金衣介绍",
    pileCount = 1,
    sellReward = {[5001]=12}
  },
  {
    id = 13,
    name = "紫金玲珑帽",
    icon = "equip_13",
    type = 2,
    refId = 13,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金玲珑帽介绍",
    pileCount = 1,
    sellReward = {[5001]=13}
  },
  {
    id = 14,
    name = "深渊权杖",
    icon = "equip_14",
    type = 2,
    refId = 14,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "深渊权杖介绍",
    pileCount = 1,
    sellReward = {[5001]=14}
  },
  {
    id = 15,
    name = "紫金项链",
    icon = "equip_15",
    type = 2,
    refId = 15,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金项链介绍",
    pileCount = 1,
    sellReward = {[5001]=15}
  },
  {
    id = 16,
    name = "紫金靴",
    icon = "equip_16",
    type = 2,
    refId = 16,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "紫金靴介绍",
    pileCount = 1,
    sellReward = {[5001]=16}
  },
  {
    id = 5001,
    name = "金币",
    icon = "item_5001",
    type = 3,
    refId = 0,
    handlerConf = {
      handlerType = 1,
      params = "200"
    },
    desc = "金币",
    pileCount = 999999,
    sellReward = {[5001]=17}
  },
  {
    id = 5002,
    name = "钻石",
    icon = "item_5002",
    type = 3,
    refId = 0,
    handlerConf = {
      handlerType = 1,
      params = "300"
    },
    desc = "钻石",
    pileCount = 999999,
    sellReward = {[5001]=18}
  },
  {
    id = 5003,
    name = "经验",
    icon = "item_5003",
    type = 3,
    refId = 0,
    handlerConf = {
      handlerType = 0,
      params = "0"
    },
    desc = "帮助角色升级获得更好的属性",
    pileCount = 999999,
    sellReward = {}
  },
  {
    id = 10001,
    name = "大还丹",
    icon = "item_10001",
    type = 1,
    refId = 0,
    handlerConf = {
      handlerType = 1,
      params = "300"
    },
    desc = "大回血丹",
    pileCount = 50,
    sellReward = {[5001]=19}
  },
  {
    id = 10002,
    name = "经验丹",
    icon = "item_10002",
    type = 1,
    refId = 0,
    handlerConf = {
      handlerType = 2,
      params = "300"
    },
    desc = "可以一次性增加300经验",
    pileCount = 50,
    sellReward = {[5001]=20}
  },
  {
    id = 10003,
    name = "小还丹",
    icon = "item_10003",
    type = 1,
    refId = 0,
    handlerConf = {
      handlerType = 1,
      params = "200"
    },
    desc = "回血丹",
    pileCount = 50,
    sellReward = {[5001]=21}
  }
}
---auto generate end---
local itemTable = {}
function ItemsConfig.OnFirstImport()
    itemTable = {}
    for _, item in ipairs(ItemsConfig.data) do
        local rawData = item
        item = ItemClass:New(item)
        item.rawData = rawData
        local handler = BaseUseHandler.allUseHandler[item.handlerConf.handlerType]
        if handler then
            handler = handler:New()
            handler:Parse(item.handlerConf.params)
            item.handler = handler
        end
        itemTable[item.id] = item
    end
end
---@return ItemClass
function ItemsConfig.getItemByItemNo(itemNo)
    return itemTable[itemNo]
end

return ItemsConfig