---auto generate begin---
StagesConfig = {}
StagesConfig.data = {
  {
    id = 1,
    name = "ChooseStage",
    type = 1,
    modelName = "stage/ChooseStage+ChooseStage",
    camera_initPosition = "0+0.8+3.5",
    camera_initRotation = "0+180+0",
    loadingBG = "LoadingPackage+BG_login",
    birthPosition = "0+0+0",
    birthRotation = "0+0+0",
    birthScale = "0+0+0",
    npcs = {}
  },
  {
    id = 2,
    name = "CityStage",
    type = 2,
    modelName = "stage/CityStage+CityStage",
    camera_initPosition = "0+7+10",
    camera_initRotation = "30+-190+1",
    loadingBG = "LoadingPackage+BG_login",
    birthPosition = "93+0+18",
    birthRotation = "0+270+0",
    birthScale = "2+2+2",
    npcs = {1,2}
  }
}
---auto generate end---
local serverTable = {}
function StagesConfig.OnFirstImport()
    serverTable = {}
    for _, stageConfig in ipairs(StagesConfig.data) do
        serverTable[stageConfig.id] = stageConfig
    end
end

function StagesConfig.GetStageById(id)
    return serverTable[id]
end

return StagesConfig