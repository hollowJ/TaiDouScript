---auto generate begin---
MainMenusConfig = {}
MainMenusConfig.data = {
  {
    id = 1,
    name = "背包",
    icon = "menu_bag",
    sortFactor = 1
  },
  {
    id = 2,
    name = "任务",
    icon = "menu_task",
    sortFactor = 2
  },
  {
    id = 3,
    name = "技能",
    icon = "menu_skill",
    sortFactor = 3
  },
  {
    id = 4,
    name = "战斗",
    icon = "menu_fight",
    sortFactor = 4
  },
  {
    id = 5,
    name = "商城",
    icon = "menu_shop",
    sortFactor = 5
  },
  {
    id = 6,
    name = "系统",
    icon = "menu_system",
    sortFactor = 6
  }
}
---auto generate end---
local MainMenuTable
function MainMenusConfig.OnFirstImport()
    MainMenuTable = {}
    table.sort(MainMenusConfig.data, function(a, b)
        return a.sortFactor > b.sortFactor
    end)
    for _, mainMenu in ipairs(MainMenusConfig.data) do
        MainMenuTable[mainMenu.id] = mainMenu
    end
end
function MainMenusConfig.GetMenuConfigByIndex(index)
    return MainMenusConfig.data[index]
end
return MainMenusConfig