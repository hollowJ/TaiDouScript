---auto generate begin---
ServersConfig = {}
ServersConfig.data = {
  {
    id = 1,
    serverName = "1区 亢龙有悔",
    status = 1,
    systemNotice = "[color=#00CC00]雷霆战绩12月10日开启活动如下：[/color]\n1.首充6元送168钻石\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，首充充值6元增设168钻石\n[color=#FF00FF]备注：啦啦啦[/color]\n2.累计充值送刀锋\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，累计充值88元，赠送：3星异界刀锋*1\n[color=#FF00FF]备注：啦啦啦[/color]\n3.每日寻宝箱得大奖\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，每天第2次购买经验核心\n[color=#FF00FF]备注：啦啦啦[/color]"
  },
  {
    id = 2,
    serverName = "2区 霍金",
    status = 2,
    systemNotice = "[color=#00CC00]雷霆战绩12月10日开启活动如下：[/color]\n1.首充6元送168钻石\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，首充充值6元增设168钻石\n[color=#FF00FF]备注：啦啦啦[/color]\n2.累计充值送刀锋\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，累计充值88元，赠送：3星异界刀锋*1\n[color=#FF00FF]备注：啦啦啦[/color]\n3.每日寻宝箱得大奖\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，每天第2次购买经验核心\n[color=#FF01FF]备注：啦啦啦[/color]"
  },
  {
    id = 3,
    serverName = "3区 齐德龙东强",
    status = 1,
    systemNotice = "[color=#00CC00]雷霆战绩12月10日开启活动如下：[/color]\n1.首充6元送168钻石\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，首充充值6元增设168钻石\n[color=#FF00FF]备注：啦啦啦[/color]\n2.累计充值送刀锋\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，累计充值88元，赠送：3星异界刀锋*1\n[color=#FF00FF]备注：啦啦啦[/color]\n3.每日寻宝箱得大奖\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，每天第2次购买经验核心\n[color=#FF02FF]备注：啦啦啦[/color]"
  },
  {
    id = 4,
    serverName = "4区 飞龙在天",
    status = 1,
    systemNotice = "[color=#00CC00]雷霆战绩12月10日开启活动如下：[/color]\n1.首充6元送168钻石\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，首充充值6元增设168钻石\n[color=#FF00FF]备注：啦啦啦[/color]\n2.累计充值送刀锋\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，累计充值88元，赠送：3星异界刀锋*1\n[color=#FF00FF]备注：啦啦啦[/color]\n3.每日寻宝箱得大奖\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，每天第2次购买经验核心\n[color=#FF03FF]备注：啦啦啦[/color]"
  },
  {
    id = 5,
    serverName = "5区 赤子之心",
    status = 2,
    systemNotice = "[color=#00CC00]雷霆战绩12月10日开启活动如下：[/color]\n1.首充6元送168钻石\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，首充充值6元增设168钻石\n[color=#FF00FF]备注：啦啦啦[/color]\n2.累计充值送刀锋\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，累计充值88元，赠送：3星异界刀锋*1\n[color=#FF00FF]备注：啦啦啦[/color]\n3.每日寻宝箱得大奖\n时间：12月16日凌晨3点-12月18日24点\n内容：活动期间，每天第2次购买经验核心\n[color=#FF04FF]备注：啦啦啦[/color]"
  }
}
---auto generate end---
local ServerList
function ServersConfig.OnFirstImport()
    ServerList = {}
    for _, serverConfig in ipairs(ServersConfig.data) do
        ServerList[serverConfig.id] = serverConfig
    end
end
function ServersConfig.getServerById(id)
    return ServerList[id]
end

function ServersConfig.getHotServer()
    return ServersConfig.data[1]
end

function ServersConfig.getServerList()
    return ServersConfig.data
end
return ServersConfig