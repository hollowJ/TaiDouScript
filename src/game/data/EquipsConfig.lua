---auto generate begin---
EquipsConfig = {}
EquipsConfig.data = {
  {
    id = 1,
    name = "青铜手镯",
    job = 1,
    icon = "equip1",
    baseAttr = {
      hp = 500,
      attack = 31
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-0"
    },
    extraAttrNum = "1",
    pos = 6,
    itemId = 1,
    starNum = 1
  },
  {
    id = 2,
    name = "完好的翅膀",
    job = 1,
    icon = "equip2",
    baseAttr = {
      hp = 500,
      attack = 32
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-1"
    },
    extraAttrNum = "1-3",
    pos = 8,
    itemId = 2,
    starNum = 2
  },
  {
    id = 3,
    name = "青铜戒指",
    job = 1,
    icon = "equip3",
    baseAttr = {
      hp = 500,
      attack = 33
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-2"
    },
    extraAttrNum = "2",
    pos = 7,
    itemId = 3,
    starNum = 3
  },
  {
    id = 4,
    name = "青铜盔甲",
    job = 1,
    icon = "equip4",
    baseAttr = {
      hp = 500,
      attack = 34
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-3"
    },
    extraAttrNum = "3",
    pos = 2,
    itemId = 4,
    starNum = 4
  },
  {
    id = 5,
    name = "青铜帽子",
    job = 1,
    icon = "equip5",
    baseAttr = {
      hp = 500,
      attack = 35
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-4"
    },
    extraAttrNum = "4",
    pos = 1,
    itemId = 5,
    starNum = 5
  },
  {
    id = 6,
    name = "青铜斧头",
    job = 1,
    icon = "equip6",
    baseAttr = {
      hp = 500,
      attack = 36
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-5"
    },
    extraAttrNum = "4",
    pos = 3,
    itemId = 6,
    starNum = 1
  },
  {
    id = 7,
    name = "青铜项链",
    job = 1,
    icon = "equip7",
    baseAttr = {
      hp = 500,
      attack = 37
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-6"
    },
    extraAttrNum = "4",
    pos = 5,
    itemId = 7,
    starNum = 2
  },
  {
    id = 8,
    name = "青铜鞋子",
    job = 1,
    icon = "equip8",
    baseAttr = {
      hp = 500,
      attack = 38
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-7"
    },
    extraAttrNum = "4",
    pos = 4,
    itemId = 8,
    starNum = 3
  },
  {
    id = 9,
    name = "紫金手镯",
    job = 2,
    icon = "equip9",
    baseAttr = {
      hp = 500,
      attack = 39
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-8"
    },
    extraAttrNum = "1",
    pos = 6,
    itemId = 9,
    starNum = 4
  },
  {
    id = 10,
    name = "紫翼翅膀",
    job = 2,
    icon = "equip10",
    baseAttr = {
      hp = 500,
      attack = 40
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-9"
    },
    extraAttrNum = "1-3",
    pos = 8,
    itemId = 10,
    starNum = 5
  },
  {
    id = 11,
    name = "紫金戒指",
    job = 2,
    icon = "equip11",
    baseAttr = {
      hp = 500,
      attack = 41
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-10"
    },
    extraAttrNum = "2",
    pos = 7,
    itemId = 11,
    starNum = 1
  },
  {
    id = 12,
    name = "紫金衣",
    job = 2,
    icon = "equip12",
    baseAttr = {
      hp = 500,
      attack = 42
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-11"
    },
    extraAttrNum = "3",
    pos = 2,
    itemId = 12,
    starNum = 2
  },
  {
    id = 13,
    name = "紫金玲珑帽",
    job = 2,
    icon = "equip13",
    baseAttr = {
      hp = 500,
      attack = 43
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-12"
    },
    extraAttrNum = "1",
    pos = 1,
    itemId = 13,
    starNum = 3
  },
  {
    id = 14,
    name = "深渊权杖",
    job = 2,
    icon = "equip14",
    baseAttr = {
      hp = 500,
      attack = 44
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-13"
    },
    extraAttrNum = "2",
    pos = 3,
    itemId = 14,
    starNum = 4
  },
  {
    id = 15,
    name = "紫金项链",
    job = 2,
    icon = "equip15",
    baseAttr = {
      hp = 500,
      attack = 45
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-14"
    },
    extraAttrNum = "3",
    pos = 5,
    itemId = 15,
    starNum = 5
  },
  {
    id = 16,
    name = "紫金靴",
    job = 2,
    icon = "equip16",
    baseAttr = {
      hp = 500,
      attack = 46
    },
    extraAttr = {
      hp = "300-900",
      attack = "0-15"
    },
    extraAttrNum = "4",
    pos = 4,
    itemId = 16,
    starNum = 3
  }
}
---auto generate end---
local equipTable = {}
function EquipsConfig.OnFirstImport()
    equipTable = {}
    for _, equip in ipairs(EquipsConfig.data) do
        equipTable[equip.id] = equip
    end
end
function EquipsConfig.getEquipByEquipId(equipId)
    return equipTable[equipId]
end
return EquipsConfig