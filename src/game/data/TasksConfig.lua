---auto generate begin---
TasksConfig = {}
TasksConfig.data = {
  {
    id = 1,
    type = 1,
    taskName = "试炼之地",
    taskDesc = "通过试炼之地完成新手挑战",
    taskTarget = "passBarr",
    taskArgs = {brrNo=1,count=1},
    reward = {[5001]=100,[5003]=100,[1]=1},
    limitLv = 1,
    equipItemNo = 1,
    goldNum = 10,
    crystalNum = 11,
    goTarget = {1,1},
    takeCondition = {type=0,args=0}
  },
  {
    id = 2,
    type = 2,
    taskName = "每日通关",
    taskDesc = "通关任意关卡一次",
    taskTarget = "passBarr",
    taskArgs = {count=1},
    reward = {[5001]=100,[5003]=300,[3]=1},
    limitLv = 3,
    equipItemNo = 2,
    goldNum = 11,
    crystalNum = 12,
    goTarget = {0,0},
    takeCondition = {type=1,args=1}
  },
  {
    id = 3,
    type = 3,
    taskName = "赏金猎人",
    taskDesc = "通过赏金之地抓捕阿凡达",
    taskTarget = "passBarr",
    taskArgs = {brrNo=2,count=2},
    reward = {[5001]=100,[5003]=200,[2]=1},
    limitLv = 2,
    equipItemNo = 3,
    goldNum = 12,
    crystalNum = 13,
    goTarget = {1,2},
    takeCondition = {type=2,args=2}
  }
}
---auto generate end---
local tasksData = {}
local MainTasks = {}
local DailyTasks = {}
local RewardTasks = {}
local typeTask = {}
function TasksConfig.OnFirstImport()
    for _, task in ipairs(TasksConfig.data) do
        tasksData[task.id] = task
        if task.type == 1 then
            table.insert(MainTasks, task)
        elseif task.type == 2 then
            table.insert(DailyTasks, task)
        elseif task.type == 3 then
            table.insert(RewardTasks, task)
        end
    end
    typeTask[1] = MainTasks
    typeTask[2] = DailyTasks
    typeTask[3] = RewardTasks
end
function TasksConfig.GetTaskById(id)
    return tasksData[id]
end
function TasksConfig.GetTasksByType(type)
    return typeTask[type]
end
return TasksConfig