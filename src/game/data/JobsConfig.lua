---auto generate begin---
JobsConfig = {}
JobsConfig.data = {
  {
    id = 1,
    name = "侠客",
    modelUIName = "model_boy_UI",
    modelName = "model_boy",
    position = "210+-500+31",
    scale = "380+380+380",
    rotation = "0+-180+0",
    icon = "CharacterPackage+icon_boy"
  },
  {
    id = 2,
    name = "魅影",
    modelUIName = "model_girl_UI",
    modelName = "model_girl",
    position = "210+-500+32",
    scale = "400+400+400",
    rotation = "0+-180+0",
    icon = "CharacterPackage+icon_girl"
  }
}
---auto generate end---
local jobsData
function JobsConfig.OnFirstImport()
  jobsData={}
  for _, jobConfig in ipairs(JobsConfig.data) do
    jobsData[jobConfig.id] = jobConfig
  end
end
function JobsConfig.GetJobById(id)
  return jobsData[id]
end
return JobsConfig