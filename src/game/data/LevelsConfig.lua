---auto generate begin---
LevelsConfig = {}
LevelsConfig.data = {
  {
    lv = 1,
    nextLvExp = 50,
    maxTrainNum = 50,
    hpRecoverySpeed = 5,
    trainRecoverySpeed = 1,
    baseAttr = {
      hp = 200,
      attack = 100,
      defence = 50
    }
  },
  {
    lv = 2,
    nextLvExp = 100,
    maxTrainNum = 50,
    hpRecoverySpeed = 5,
    trainRecoverySpeed = 1,
    baseAttr = {
      hp = 300,
      attack = 1550,
      defence = 70
    }
  },
  {
    lv = 3,
    nextLvExp = 150,
    maxTrainNum = 51,
    hpRecoverySpeed = 6,
    trainRecoverySpeed = 2,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 90
    }
  },
  {
    lv = 4,
    nextLvExp = 200,
    maxTrainNum = 52,
    hpRecoverySpeed = 7,
    trainRecoverySpeed = 3,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 91
    }
  },
  {
    lv = 5,
    nextLvExp = 250,
    maxTrainNum = 53,
    hpRecoverySpeed = 8,
    trainRecoverySpeed = 4,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 92
    }
  },
  {
    lv = 6,
    nextLvExp = 300,
    maxTrainNum = 54,
    hpRecoverySpeed = 9,
    trainRecoverySpeed = 5,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 93
    }
  },
  {
    lv = 7,
    nextLvExp = 350,
    maxTrainNum = 55,
    hpRecoverySpeed = 10,
    trainRecoverySpeed = 6,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 94
    }
  },
  {
    lv = 8,
    nextLvExp = 400,
    maxTrainNum = 56,
    hpRecoverySpeed = 11,
    trainRecoverySpeed = 7,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 95
    }
  },
  {
    lv = 9,
    nextLvExp = 450,
    maxTrainNum = 57,
    hpRecoverySpeed = 12,
    trainRecoverySpeed = 8,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 96
    }
  },
  {
    lv = 10,
    nextLvExp = 500,
    maxTrainNum = 58,
    hpRecoverySpeed = 13,
    trainRecoverySpeed = 9,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 97
    }
  },
  {
    lv = 11,
    nextLvExp = 550,
    maxTrainNum = 59,
    hpRecoverySpeed = 14,
    trainRecoverySpeed = 10,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 98
    }
  },
  {
    lv = 12,
    nextLvExp = 600,
    maxTrainNum = 60,
    hpRecoverySpeed = 15,
    trainRecoverySpeed = 11,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 99
    }
  },
  {
    lv = 13,
    nextLvExp = 650,
    maxTrainNum = 61,
    hpRecoverySpeed = 16,
    trainRecoverySpeed = 12,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 100
    }
  },
  {
    lv = 14,
    nextLvExp = 700,
    maxTrainNum = 62,
    hpRecoverySpeed = 17,
    trainRecoverySpeed = 13,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 101
    }
  },
  {
    lv = 15,
    nextLvExp = 750,
    maxTrainNum = 63,
    hpRecoverySpeed = 18,
    trainRecoverySpeed = 14,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 102
    }
  },
  {
    lv = 16,
    nextLvExp = 800,
    maxTrainNum = 64,
    hpRecoverySpeed = 19,
    trainRecoverySpeed = 15,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 103
    }
  },
  {
    lv = 17,
    nextLvExp = 850,
    maxTrainNum = 65,
    hpRecoverySpeed = 20,
    trainRecoverySpeed = 16,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 104
    }
  },
  {
    lv = 18,
    nextLvExp = 900,
    maxTrainNum = 66,
    hpRecoverySpeed = 21,
    trainRecoverySpeed = 17,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 105
    }
  },
  {
    lv = 19,
    nextLvExp = 950,
    maxTrainNum = 67,
    hpRecoverySpeed = 22,
    trainRecoverySpeed = 18,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 106
    }
  },
  {
    lv = 20,
    nextLvExp = 1000,
    maxTrainNum = 68,
    hpRecoverySpeed = 23,
    trainRecoverySpeed = 19,
    baseAttr = {
      hp = 400,
      attack = 200,
      defence = 107
    }
  }
}
---auto generate end---
local LevelsData
function LevelsConfig.OnFirstImport()
  LevelsData={}
  for _, levelConfig in ipairs(LevelsConfig.data) do
    LevelsData[levelConfig.lv] = levelConfig
  end
end
function LevelsConfig.GetLevelConfigByLv(lv)
  return LevelsData[lv]
end
return LevelsConfig