---auto generate begin---
NpcTalksConfig = {}
NpcTalksConfig.data = {
    {
        id = 1,
        sort = "1",
        talkMsg = "你好，勇士，准备好开始了吗？",
        npcId = 1
    },
    {
        id = 1,
        sort = "2",
        talkMsg = "你好，我准备好啦",
        npcId = -1
    },
    {
        id = 1,
        sort = "3",
        talkMsg = "拭目以待",
        npcId = 1
    },
    {
        id = 2,
        sort = "1",
        talkMsg = "年轻人，我的xx被妖怪抓走啦，你愿意帮我找回吗？",
        npcId = 1
    },
    {
        id = 2,
        sort = "2",
        talkMsg = "等着吧",
        npcId = -1
    },
    {
        id = 2,
        sort = "3",
        talkMsg = "我等你哟",
        npcId = 1
    }
}
---auto generate end---
local npcTalkGroup = {}
function NpcTalksConfig.GetNpcGroupTalkById(talkNo)
    return npcTalkGroup[talkNo]
end

function NpcTalksConfig.OnFirstImport()
    for _, talk in ipairs(NpcTalksConfig.data) do
        local talkNo = talk.id
        local groupTalkData = npcTalkGroup[talkNo]
        if not groupTalkData then
            groupTalkData = {}
            npcTalkGroup[talkNo] = groupTalkData
        end
        groupTalkData[talk.sort] = talk
    end
    for talkNo, talkGroup in ipairs(npcTalkGroup) do
        table.sort(talkGroup, function(a, b)
            return a.sort < b.sort
        end)
    end
end
return NpcTalksConfig