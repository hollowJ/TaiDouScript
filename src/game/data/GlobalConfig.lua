---auto generate begin---
GlobalsConfig = {}
GlobalsConfig.data = {
  NameLimit = 7,
  LevelRange = {
    Min = 1,
    Max = 99
  }
}
---auto generate end---
return GlobalsConfig