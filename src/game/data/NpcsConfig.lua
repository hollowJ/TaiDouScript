---auto generate begin---
NpcsConfig = {}
NpcsConfig.data = {
  {
    id = 1,
    name = "女npc",
    modelName = "npc_1",
    position = {
      x = 76,
      y = 0,
      z = -10
    },
    scale = {
      x = 2,
      y = 2,
      z = 2
    },
    rotation = {
      x = 0,
      y = 0,
      z = 0
    }
  },
  {
    id = 2,
    name = "男npc",
    modelName = "npc_2",
    position = {
      x = -9,
      y = 0,
      z = -12
    },
    scale = {
      x = 2,
      y = 2,
      z = 2
    },
    rotation = {
      x = 0,
      y = 0,
      z = 0
    }
  }
}
---auto generate end---
local npcTable = {}
function NpcsConfig.OnFirstImport()
  npcTable = {}
  for _, npcConfig in ipairs(NpcsConfig.data) do
    npcTable[npcConfig.id] = npcConfig
  end
end

function NpcsConfig.GetNpcById(id)
  return npcTable[id]
end

return NpcsConfig