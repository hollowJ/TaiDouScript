---@class ChooseCharacterWindow:BaseWindow
ChooseCharacterWindow = fgui.window_class(BaseWindow)
local selectDefault = 1


--可覆盖的函数(可选，不是说必须）
function ChooseCharacterWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_chooseCharacter")
    self.btn_enterGame = self.contentPane:GetChild("btn_enterGame")
    self.btn_backLogin = self.contentPane:GetChild("btn_back")

    self.txt_sysNotice = self.contentPane:GetChild("com_noticeContent")
    self.graph_holder = self.contentPane:GetChild("model_holder_cloud")
                            :GetChild("model_holder"):GetChild("graph_holder")
    self.btn_changeCharacter = self.contentPane:GetChild("btn_changeCharacter")
    self.list_role = self.contentPane:GetChild("list_role")
    self.goWrapper = GoWrapper.New()
    self.graph_holder:SetNativeObject(self.goWrapper)
end

function ChooseCharacterWindow:OnHide()
    CharacterController:RecycleCharacterModel(ChooseCharacterWindow.goWrapper)
end
function ChooseCharacterWindow:DoShowAnimation()
    self:OnShown()
end
function ChooseCharacterWindow:DoHideAnimation()
    self:HideImmediately()
end
local function chooseRoleModel(role)
    CharacterController:SetUpCharacterModel(role.base.job, ChooseCharacterWindow.goWrapper)
end
local function toCreate()
    ModuleEvent.Dispatch(ModuleConst.TO_CREATE_CHARACTER)
    WindowManager.OpenWindow("CreateCharacterWindow2")
end
local function toChoose(eventContext)
    local sender = eventContext.sender
    local index = sender.data[1]
    local role = sender.data[2]
    selectDefault = index + 1
    chooseRoleModel(role)
    RoleInfoModel:setCurrentRole(role)

end
function ChooseCharacterWindow:OnShown()
    local rolelist = TableUtil.shallow_copy(RoleInfoModel:getRoleInfolist())
    local createRole = { base = { createFlag = 1 } }
    table.insert(rolelist, createRole)
    ChooseCharacterWindow.list_role.itemRenderer = function(index, item_role)
        local role=rolelist[index + 1]
        local base = TableUtil.deep_copy(rolelist[index + 1].base)
        extends(base,BaseRoleClass)
        item_role.data = nil
        item_role.data = { index, role }
        local group_showRole = item_role:GetChild("group_showRole")
        local group_create = item_role:GetChild("group_create")
        local loader_icon = item_role:GetChild("loader_icon")

        if base.createFlag then
            group_showRole.visible = false
            group_create.visible = true
            loader_icon.visible = false
            item_role.onClick:Add(toCreate)
        else
            local jobConfig = JobsModel.GetJobById(base.job)
            group_showRole.visible = true
            group_create.visible = false
            local txt_name = item_role:GetChild("txt_name")
            local txt_lv = item_role:GetChild("txt_lv")
            txt_lv.text = "LV." .. base.lv
            txt_name.text = base.name
            local iconArr = StringUtil.split(jobConfig.icon, "+")
            loader_icon.url = ResourceManager.getResourceURL(iconArr[1], iconArr[2])
            item_role.onClick:Add(toChoose)
        end
        if index + 1 == selectDefault then
            item_role.onClick:Call()
        end
    end
    ChooseCharacterWindow.list_role.numItems = #rolelist
    ChooseCharacterWindow.txt_sysNotice.title = ServerModel.getCurServer().systemNotice
end
local function OnBackLogin()
    ChooseCharacterWindow:CloseWindow()
    WindowManager.OpenWindow("LoginMainWindow")

end
local function onCreateCharacter()
    ChooseCharacterWindow:CloseWindow()
end
local function onEnterGame()
    ModuleEvent.Dispatch(ModuleConst.START_GAME)
    player = PlayerClass:Create(RoleInfoModel:getCurrentRole())
end
local function onStartGame()
    ChooseCharacterWindow:CloseWindow()
    ModuleEvent.Dispatch(ModuleConst.LOAD_STAGE_START, StageConst.SafeCity)
end
function ChooseCharacterWindow:OnRegisterEvent()
    self.btn_backLogin.onClick:Add(OnBackLogin)
    self.btn_enterGame.onClick:Add(onEnterGame)
    ModuleEvent.AddListenner(ModuleConst.TO_CREATE_CHARACTER, onCreateCharacter)
    ModuleEvent.AddListenner(ModuleConst.START_GAME, onStartGame)

end
return ChooseCharacterWindow