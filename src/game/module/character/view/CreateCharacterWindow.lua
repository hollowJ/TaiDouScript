---@class CreateCharacterWindow:BaseWindow
CreateCharacterWindow = fgui.window_class(BaseWindow)

--可覆盖的函数(可选，不是说必须）
function CreateCharacterWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_createCharacter")
    self.btn_backChoose = self.contentPane:GetChild("btn_back")
    self.list_job = self.contentPane:GetChild("list_job")
end

function CreateCharacterWindow:OnShown()
    self.list_job.itemRenderer = function(index, item)
        local jobConfig = JobsModel.getJobByIndex(index + 1)
        if not ( item.data and item.data["goWrapper"]) then
            local model_holder = item:GetChild("model_holder")
            local graph_holder = model_holder:GetChild("graph_holder")
            item.data = {}
            item.data["goWrapper"] = GoWrapper.New()
            graph_holder:SetNativeObject(item.data["goWrapper"])
        end
        if item.data["goWrapper"].wrapTarget then
            ObjectPool:recycle(item.data["goWrapper"].wrapTarget.name, item.data["goWrapper"].wrapTarget)
        end
        CharacterController:SetUpCharacterModel(jobConfig.id, item.data["goWrapper"])
    end
    self.list_job.numItems = #JobsModel.GetAllJob()
end
function CreateCharacterWindow:OnHide()
    for index = 1, self.list_job.numItems do
        local item = self.list_job:GetChildAt(index-1)
        if item.data and item.data["goWrapper"] then
            CharacterController:RecycleCharacterModel(item.data["goWrapper"])
        end
    end

end
function CreateCharacterWindow:DoShowAnimation()
    self:OnShown()
end
function CreateCharacterWindow:DoHideAnimation()
    self:HideImmediately()
end
local function OnBackChoose()
    CreateCharacterWindow:CloseWindow()
    WindowManager.OpenWindow("ChooseCharacterWindow")
end
local function onStartGame()
    CreateCharacterWindow:CloseWindow()
end
function CreateCharacterWindow:OnRegisterEvent()
    self.btn_backChoose.onClick:Add(OnBackChoose)
    ModuleEvent.AddListenner(ModuleConst.START_GAME, onStartGame)

end
return CreateCharacterWindow