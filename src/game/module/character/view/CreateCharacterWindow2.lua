---@class CreateCharacterWindow2:BaseWindow
CreateCharacterWindow2 = fgui.window_class(BaseWindow)
local selectDefault = 1
local model
--可覆盖的函数(可选，不是说必须）
function CreateCharacterWindow2:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_createCharacter2")

    self.btn_backChoose = self.contentPane:GetChild("btn_back")
    self.list_job = self.contentPane:GetChild("list_job")
    self.graph_touch = self.contentPane:GetChild("graph_touch")
    self.btn_enterGame = self.contentPane:GetChild("btn_enterGame")
    self.txt_userName = self.contentPane:GetChild("txt_userName")
end
local function toChoose(eventContext)
    local sender = eventContext.sender
    local index = sender.data
    local jobConfig = JobsModel.getJobByIndex(index + 1)
    selectDefault = index + 1
    local modelUIName = jobConfig.modelUIName
    model = ObjectPool:getThing(modelUIName)
    local characterPosition = GameObject.Find("CharacterPosition")
    if characterPosition.transform.childCount > 0 then
        local childTransform = characterPosition.transform:GetChild(0)
        local childObj = childTransform.gameObject
        ObjectPool:recycleObj(childObj)
    end
    LuaHelper.SetLayersRecursively(model, Layer.Default)
    model.transform:SetParent(characterPosition.transform)
    model.transform.localPosition = Vector3.New(0, 0, 0)
    model.transform.localScale = Vector3.New(1, 1, 1)
    model.transform.localRotation = Quaternion.New(0, 0, 0)

    model:SetActive(true)
end

function CreateCharacterWindow2:OnShown()
    self.list_job .itemRenderer = function(index, item_job)
        local jobConfig = JobsModel.getJobByIndex(index + 1)
        local group_showRole = item_job:GetChild("group_showRole")
        local group_create = item_job:GetChild("group_create")
        group_showRole.visible = false
        group_create.visible = false
        local loader_icon = item_job:GetChild("loader_icon")
        local iconArr = StringUtil.split(jobConfig.icon, "+")
        loader_icon.url = ResourceManager.getResourceURL(iconArr[1], iconArr[2])
        item_job.data = index
        item_job.onClick:Add(toChoose)
        if index + 1 == selectDefault then
            item_job.onClick:Call()
        end
    end
    self.list_job .numItems = #JobsModel.GetAllJob()
end
function CreateCharacterWindow2:OnHide()

end
function CreateCharacterWindow2:DoShowAnimation()
    self:OnShown()
end
function CreateCharacterWindow2:DoHideAnimation()
    self:HideImmediately()
end
local function OnBackChoose()
    CreateCharacterWindow2:CloseWindow()
    WindowManager.OpenWindow("ChooseCharacterWindow")
end
local function onEnterGame()
    local userName = CreateCharacterWindow2.txt_userName.title
    local base = {}
    base.job = JobsModel.getJobByIndex(selectDefault).id
    base.name = userName
    local roleInfo = {}
    roleInfo.base = base
    CharacterController:CreateCharacter(roleInfo)
    player = PlayerClass:Create(RoleInfoModel:getCurrentRole())
    ModuleEvent.Dispatch(ModuleConst.START_GAME)

end
local function onStartGame()
    CreateCharacterWindow2:CloseWindow()
end

function CreateCharacterWindow2:OnRegisterEvent()
    self.graph_touch.onTouchBegin:Add(function(context)
        local event = context.inputEvent
        local beginPosition = {}
        beginPosition.x = event.x
        beginPosition.y = event.y
        context.sender.data = beginPosition
        context:CaptureTouch()
    end)
    self.graph_touch.onTouchMove:Add(function(context)
        local event = context.inputEvent
        local endPosition = {}
        endPosition.x = event.x
        endPosition.y = event.y
        local delta = endPosition.x - context.sender.data.x
        local rotateSpeed = -1 * delta
        local localEulerAngles = model.transform.localEulerAngles
        model.transform.localEulerAngles = Vector3.New(localEulerAngles.x, localEulerAngles.y + rotateSpeed, localEulerAngles.z)
        context.sender.data = endPosition
    end)
    self.btn_backChoose.onClick:Add(OnBackChoose)
    self.btn_enterGame.onClick:Add(onEnterGame)
    ModuleEvent.AddListenner(ModuleConst.START_GAME, onStartGame)

end
return CreateCharacterWindow2