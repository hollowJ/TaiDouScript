---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by hollowJ.
--- DateTime: 2018/8/30 21:57
---
MainPlayer = {}
---@type UnityEngine.GameObject
MainPlayer.model = nil
---@type UnityEngine.Camera
local MainCameraCtn = nil
local speed = 3
---@type UnityEngine.GameObject
local CameraFollowAndRotate = nil
---@type UnityEngine.GameObject
local CameraUpAndDown = nil
---@type UnityEngine.GameObject
local CameraZoom = nil
---@type UnityEngine.Camera
MainCamera = nil
function MainPlayer.onUpdate()
    MainPlayer.Move()
end
function MainPlayer.onLateUpdate()
    MainPlayer.CameraControl()
end

function MainPlayer.Move()
    local h = Input.GetAxis("Horizontal")
    local v = Input.GetAxis("Vertical")
    MainPlayer.rigidbody.velocity = Vector3.New(-h, MainPlayer.rigidbody.velocity.y, -v) * speed
    if math.abs(h) > 0.5 or math.abs(v) > 0.5 then
        MainPlayer.transform.rotation = Quaternion.LookRotation(Vector3.New(-h, 0, -v))
        if MainPlayer.navMeshAgent.enabled then
            MainPlayer.StopAutoFindPath()
        end
        MainPlayer.StartWalkAnim()
    else
        if not MainPlayer.navMeshAgent.enabled and h == 0 and v == 0 then
            MainPlayer.rigidbody.velocity = Vector3.New(0, 0, 0)
            MainPlayer.StopWalkAnim()
        end
    end
    MainPlayer.lastPosition = MainPlayer.transform.position
end
function MainPlayer.CameraControl()
    local mouseX = Input.GetAxis("Mouse X")
    local mouseY = Input.GetAxis("Mouse Y")
    local scrollWheelAxis = Input.GetAxis("Mouse ScrollWheel")
    if Input.GetMouseButton(1) then
        local speed = 30
        CameraFollowAndRotate.transform.localEulerAngles = Vector3.New(CameraFollowAndRotate.transform.localEulerAngles.x, CameraFollowAndRotate.transform.localEulerAngles.y + speed * mouseX * Time.deltaTime, CameraFollowAndRotate.transform.localEulerAngles.z)
        CameraUpAndDown.transform.localEulerAngles = Vector3.New(CameraUpAndDown.transform.localEulerAngles.x + speed * mouseY * Time.deltaTime, CameraUpAndDown.transform.localEulerAngles.y, CameraUpAndDown.transform.localEulerAngles.z)
    end
    if scrollWheelAxis ~= 0 then
        CameraZoom.transform.localPosition = Vector3.New(CameraZoom.transform.localPosition.x, CameraZoom.transform.localPosition.y, CameraZoom.transform.localPosition.z + -speed * scrollWheelAxis)
    end
    CameraFollowAndRotate.transform.position = MainPlayer.transform.position

end
function MainPlayer.Init(model)
    MainPlayer.model = model
    MainPlayer.lastPosition = nil
    MainPlayer.animator = MainPlayer.model:GetComponent("Animator")
    MainPlayer.rigidbody = MainPlayer.model:GetComponent("Rigidbody")
    ---@type UnityEngine.Transform
    MainPlayer.transform = MainPlayer.model:GetComponent("Transform")
    ---@type UnityEngine.AI.NavMeshAgent
    MainPlayer.navMeshAgent = MainPlayer.model:GetComponent("NavMeshAgent")
    MainPlayer.lastPosition = MainPlayer.transform.position
    MainPlayer.navMeshAgent.enabled = false
    MainCameraCtn = GameObject.Find("MainCameraContainer")
    CameraFollowAndRotate = GameObject.Find("CameraFollowAndRotate")
    CameraFollowAndRotate.transform.position = MainPlayer.transform.position

    CameraUpAndDown = GameObject.Find("CameraUpAndDown")
    MainCamera = GameObject.Find("MainCamera"):GetComponent("Camera")
    CameraZoom = GameObject.Find("CameraZoom")
    local stageConfig = StageManager.curStage.stageConfig
    local positionArr = StringUtil.split(stageConfig.camera_initPosition, "+", tonumber)
    MainCameraCtn.transform.localPosition = Vector3.New(positionArr[1], positionArr[2], positionArr[3])
    print("mainplayerr init ",MainPlayer.transform.position,CameraFollowAndRotate.transform.position)
    MainCameraCtn.transform:LookAt(MainPlayer.transform)
    FrameTimerManager.RegisterUpdate(MainPlayer.onUpdate)
    FrameTimerManager.RegisterLateUpdate(MainPlayer.onLateUpdate)

end
function MainPlayer.StopWalkAnim()
    --print("MainPlayer.StopWalk")
    MainPlayer.animator:SetBool("walk", false)
end
function MainPlayer.StopAutoFindPath()
    --print("MainPlayer.StopAutoFindPath")
    MainPlayer.navMeshAgent.enabled = false
    MainPlayer.StopWalkAnim()
end
function MainPlayer.StartWalkAnim()
    --print("MainPlayer.StartWalk")
    MainPlayer.animator:SetBool("walk", true)
end