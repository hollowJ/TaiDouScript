--- @class GmWindow : BaseWindow
GmWindow = fgui.window_class(BaseWindow)
GmWindow.windowName = nil

--可覆盖的函数(可选，不是说必须）
function GmWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("ChatPackage", "win_gm")
    self.modal = true
    self.txt_gm = self.contentPane:GetChild("txt_gm")
    self.list_gm = self.contentPane:GetChild("list")
    self.txt_title = self.contentPane:GetChild("txt_gm"):GetChild("title")
    self:Center()
end
local function updateGMText(gmText)
    GmWindow.txt_gm.title = gmText
    GmWindow.txt_title:RequestFocus()
    GmWindow.txt_title.caretPosition = string.len(gmText)

end
local function onGmClick(context)
    local selectedItem = context.sender;
    updateGMText(selectedItem.title)
end
local function updateGmHistory()
    local historys = TableUtil.wrap(GmModel.gmHistory)
    table.sort(historys, function(a, b)
        return a.value > b.value
    end)
    GmWindow.list_gm.itemRenderer = function(index, item)
        item.title = historys[index + 1].key
        item.onClick:Add(onGmClick)
    end
    GmWindow.list_gm.numItems = #historys
    GmWindow.list_gm.selectedIndex = -1;
end
function GmWindow:OnShown()
    updateGmHistory()
    updateGMText(" ")
end
function GmWindow:OnHide()

end
function GmWindow:ActionEvent(context)
    local sender = context.sender
    if string.find(sender.name, "equip_") then
    elseif sender == GmWindow.equip_2 then
    end
end
local function submitGM()
    local gm = StringUtil.trim(GmWindow.txt_gm.title)
    GmWindow.txt_gm.title = ""
    if gm and gm ~= "" then
        GmController.handleGM(gm)
        GmModel.gmHistory[gm] = (GmModel.gmHistory[gm] or 0) + 1
    end
    if not WindowManager.IsShow(GmWindow.windowName) then
        WindowManager.OpenWindow(GmWindow.windowName)
    end
    updateGmHistory()
end
local function OnKeyDown()
    if Input.GetKeyDown(KeyCode.F1) then
        if not WindowManager.IsShow(GmWindow.windowName) then
            WindowManager.OpenWindow(GmWindow.windowName)
        else
            submitGM()
            WindowManager.CloseWindow(GmWindow.windowName)
        end
    elseif Input.GetKeyDown(KeyCode.Escape) then
        if WindowManager.IsShow(GmWindow.windowName) then
            WindowManager.CloseWindow(GmWindow.windowName)
        end
    elseif Input.GetKeyDown(KeyCode.Return) then
        if WindowManager.IsShow(GmWindow.windowName) then
            submitGM()
        end
    elseif Input.GetKeyDown(KeyCode.UpArrow) then
        if WindowManager.IsShow(GmWindow.windowName) then
            if (GmWindow.list_gm.numItems > 0) then
                GmWindow.list_gm.selectedIndex = (GmWindow.list_gm.selectedIndex - 1) % GmWindow.list_gm.numItems
                local selectedItem = GmWindow.list_gm:GetChildAt(GmWindow.list_gm.selectedIndex)
                updateGMText(selectedItem.text)
            end
        end
    elseif Input.GetKeyDown(KeyCode.DownArrow) then
        if WindowManager.IsShow(GmWindow.windowName) then
            if (GmWindow.list_gm.numItems > 0) then
                GmWindow.list_gm.selectedIndex = (GmWindow.list_gm.selectedIndex + 1) % GmWindow.list_gm.numItems
                local selectedItem = GmWindow.list_gm:GetChildAt(GmWindow.list_gm.selectedIndex)
                updateGMText(selectedItem.text)
            end
        end
    end
end
function GmWindow:OnRegisterEvent()
end

function GmWindow:OnHotUpdate()
end
local function catchErrOnKeyDown()
    --local  function keyDown()
    --    OnKeyDown()
    --end
    xpcall(OnKeyDown, function(err)
        LogHelper.Error("111111" .. err)
    end)
end
function GmWindow:OnHotUpdateBefore()
    FrameTimerManager.RemoveUpdate(catchErrOnKeyDown)
end
FrameTimerManager.RegisterUpdate(catchErrOnKeyDown)

return GmWindow