---
--- Created by hollowJ.
--- DateTime: 2018/1/14 17:49
---
WindowManager = { MAX_WINDOW_COUNT = 10 }
WindowManager.UIWindows = {}
function WindowManager.registetWindow(windowName, abPath, windowClass, layer, priority, window)
    if WindowDefine[windowName] then
        --已经注册直接返回
        return
    end
    local windowConfig = {}
    windowConfig.windowName = windowName
    windowConfig.abPath = abPath
    windowClass.abPath = abPath
    windowClass.windowName = windowName
    windowConfig.windowClass = windowClass
    windowConfig.layer = layer
    windowConfig.priority = priority
    windowConfig.window = window
    WindowDefine[windowName] = windowConfig
end

function WindowManager.OpenWindow(windowName)
    local uiWindow = WindowManager.UIWindows[windowName]
    if uiWindow then
        if uiWindow.loaded then
            uiWindow:Show()
        end
        return
    end
    local windowConfig = WindowDefine[windowName]
    uiWindow = windowConfig.windowClass.New()
    _G[windowConfig.windowName] = uiWindow
    uiWindow.isInstance = true
    WindowManager.UIWindows[windowName] = uiWindow
    uiWindow:Show()
end
function WindowManager.CloseWindow(windowName)
    local uiWindow = WindowManager.UIWindows[windowName]
    if uiWindow then
        uiWindow:Hide()
    end
end
--判断窗口是否显示
function WindowManager.IsShow(windowName)
    local uiWindow = WindowManager.UIWindows[windowName]
    if uiWindow and uiWindow.isInstance then
        return uiWindow.isShowing
    end
    return false
end
function WindowManager.DisposeWindow(windowName)
    local uiWindow = WindowManager.UIWindows[windowName]
    if not uiWindow then
        return
    end
    uiWindow:Dispose()
    WindowManager.UIWindows[windowName] = nil
end
function WindowManager.Update()
    for windowName, window in pairs(WindowManager.UIWindows) do
        if window.isInstance and WindowManager.IsShow(windowName) then
            if window.Update then
                window:Update()
            end
        end
    end
end
function WindowManager.CheckDispose()
    if TableUtil.getTableSize(WindowManager.UIWindows) > WindowManager.MAX_WINDOW_COUNT then
        local waitDestroyName
        for windowName, window in pairs(WindowManager.UIWindows) do
            local WindowInfo = WindowDefine[windowName]
            if window and WindowInfo.priority ~= 0 and not window.isShowing then
                if waitDestroyName ~= nil then
                    local waitWindowInfo = WindowDefine[waitDestroyName]
                    if WindowInfo.priority > waitWindowInfo.priority then
                        waitDestroyName = windowName
                    end
                else
                    waitDestroyName = windowName
                end
            end
        end
        if waitDestroyName then
            WindowManager.DisposeWindow(waitDestroyName)
        end
    end
end
function WindowManager.OnFirstImport()
    FrameTimerManager.RegisterUpdate(WindowManager.CheckDispose)
    FrameTimerManager.RegisterUpdate(WindowManager.Update)
end

return WindowManager