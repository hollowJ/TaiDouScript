---
--- Created by hollowJ.
--- DateTime: 2018/1/11 22:16
---

ServerModel = { class = "ServerModel" }
ServerModel = BaseModel:New(ServerModel)
local curServer
local curAccount
function ServerModel:updateCurServerId(curServerId)
    curServer = ServersConfig.getServerById(curServerId)
    if curAccount then
        curAccount.curServerId = curServerId
    end
end
function ServerModel:loadUserData()
    curAccount = AccountModel:getCurAccount()
    local curServerId
    curServer=nil
    if curAccount then
        curServerId = curAccount.curServerId
        curServer = ServersConfig.getServerById(curServerId)
    end
    if not curServer then
         curServer = ServersConfig.getHotServer()
        curServerId = curServer.id
    end
    self:updateCurServerId(curServerId)
end
function ServerModel:getCurServer()
    return curServer
end
function ServerModel:getServerList()
    return ServersConfig.getServerList()
end
return ServerModel