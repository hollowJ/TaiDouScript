---
--- Created by hollowJ.
--- DateTime: 2018/1/11 22:16
---
AccountClass = { accountName = nil, password = nil, flag = 1 }
AccountClass = Class:New(AccountClass)

AccountModel = {}
AccountModel = Class:New(AccountModel)
local accountTable
local roleContainer
function AccountModel:loadData()
    accountTable = GameData.accountTable
    if not accountTable then
        accountTable = {}
        GameData.accountTable = accountTable
    end
    local myAccount=AccountModel:getCurAccount()
    ---@type RoleContainer
    roleContainer=RoleContainer:Create(myAccount)
    roleContainer:Load(myAccount)
end
function AccountModel:verify(accountClass)
    for _, account in ipairs(accountTable) do
        if account.accountName == accountClass.accountName and accountClass.password == account.password then
            return true
        end
    end
    return false
end

function AccountModel:addAccount(accountClass)
    accountTable[accountClass.accountName] = accountClass
    return true
end

function AccountModel:getCurAccount()
    return accountTable[accountTable.curAccountName]
end

function AccountModel:getRoleContainer()
    return roleContainer
end
function AccountModel:setCurAccountName(curAccountName)
    accountTable.curAccountName = curAccountName
end
