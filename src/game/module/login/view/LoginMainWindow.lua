--- @class LoginMainWindow : BaseWindow
LoginMainWindow = fgui.window_class(BaseWindow)

local function OnServerChoose()
    LoginMainWindow.btn_selectServer.text = ServerModel:getCurServer().serverName
end

--可覆盖的函数(可选，不是说必须）
function LoginMainWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_login")
    self. btn_loginAccount = self.contentPane:GetChild("btn_loginAccount")
    self.btn_selectServer = self.contentPane:GetChild("btn_selectServer")
    self.btn_enterChooseCharacter = self.contentPane:GetChild("btn_enterChooseCharacter")
    LoginController.requireServers()
    ServerModel:loadUserData()
    OnServerChoose()
end
local function OnLoginSuccess(accountClass)
    LoginMainWindow.btn_loginAccount.text = accountClass.accountName
    ServerModel:loadUserData()
    OnServerChoose()
end
function LoginMainWindow:OnShown()
    local curAccount = AccountModel:getCurAccount()
    if not curAccount then
        curAccount = AccountClass:New()
        curAccount.accountName = "请登录"
        curAccount.flag = 0
    end
    OnLoginSuccess(curAccount)
end
function LoginMainWindow:OnHide()
end
function LoginMainWindow:DoShowAnimation()
    self:OnShown()
end
function LoginMainWindow:DoHideAnimation()
    self:HideImmediately()
end

local function OnSelectServer()
    WindowManager.OpenWindow("SelectServerWindow")
end
local function OnEnterChooseCharacter()
    WindowManager.OpenWindow("ChooseCharacterWindow")


end
local function onLoginAccount()
    WindowManager.OpenWindow("LoginWindow")
end
local function loadStageSuccess(stageId)
    if (stageId == 1) then
        WindowManager.OpenWindow("LoginMainWindow")
    end

end
local function onCreateCharacter()
    LoginMainWindow:CloseWindow()
end
local function onStartGame()
    LoginMainWindow:CloseWindow()
end
function LoginMainWindow:OnRegisterEvent()
    self.btn_loginAccount.onClick:Add(onLoginAccount)
    self.btn_enterChooseCharacter.onClick:Add(OnEnterChooseCharacter)
    self.btn_selectServer.onClick:Add(OnSelectServer)
    ModuleEvent.AddListenner(ModuleConst.LOGIN_SUCCESS, OnLoginSuccess)
    ModuleEvent.AddListenner(ModuleConst.SERVER_CHOOSE, OnServerChoose)
    ModuleEvent.AddListenner(ModuleConst.TO_CREATE_CHARACTER, onCreateCharacter)
    ModuleEvent.AddListenner(ModuleConst.START_GAME, onStartGame)

end
ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_SUCCESS, loadStageSuccess)
return LoginMainWindow