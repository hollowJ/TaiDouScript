--- @class SelectServerWindow : BaseWindow
SelectServerWindow = fgui.window_class(BaseWindow)

--构建函数
function SelectServerWindow:ctor()
end


--可覆盖的函数(可选，不是说必须）
function SelectServerWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_selectServer")
    self.com_serverItem = self.contentPane:GetChild("com_serverItem")
    self.grid_servers = self.contentPane:GetChild("grid_servers")
    self:Center()

end

function SelectServerWindow:OnShown()
    LoginController:requireServers()
end
function SelectServerWindow:OnHide()
end
--重置组建，从对象池获取需要重置
local function resetComServerItem(comItem)
    local btn_serverItem1 = comItem:GetChild(tostring(1))
    local btn_serverItem2 = comItem:GetChild(tostring(2))
    btn_serverItem1.visible = false
    btn_serverItem2.visible = false
end
local function onChooseServer()
    local curServer = ServerModel:getCurServer()
    resetComServerItem(SelectServerWindow.com_serverItem)
    local btn_serverItem = SelectServerWindow.com_serverItem:GetChild(tostring(curServer.status))
    btn_serverItem.title = curServer.serverName
    btn_serverItem.visible = true
    btn_serverItem.onClick:Clear()
    btn_serverItem.onClick:Add(function ()
        SelectServerWindow:CloseWindow()
    end)
end
local function onUpdateServers()
    local function renderItem(index, obj)
        resetComServerItem(obj)
        local ServerList = ServerModel:getServerList()
        local server = ServerList[index + 1]
        local btn_server = obj:GetChild(tostring(server.status))
        btn_server .title = server.serverName
        btn_server.visible = true
        local function chooseServer()
            ServerModel:updateCurServerId(server.id)
            ModuleEvent.Dispatch(ModuleConst.SERVER_CHOOSE)
        end
        btn_server.onClick:Clear()
        btn_server.onClick:Add(chooseServer)
    end
    SelectServerWindow.grid_servers.itemRenderer = renderItem
    SelectServerWindow.grid_servers.numItems = #ServerModel.getServerList()
end
function SelectServerWindow:OnRegisterEvent()
    ModuleEvent.AddListenner(ModuleConst.SERVER_CHOOSE, onChooseServer)
    ModuleEvent.AddListenner(ModuleConst.SERVERS_UPDATE, onUpdateServers)
    --ModuleEvent.AddListenner(ModuleConst.LOGIN_Fail, OnLoginFail)
end
return SelectServerWindow