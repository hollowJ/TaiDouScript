--- @class LoginWindow : BaseWindow
LoginWindow = fgui.window_class(BaseWindow)

--构建函数
function LoginWindow:ctor()
end


--可覆盖的函数(可选，不是说必须）
function LoginWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_loginDialog")
    self.btn_register = self.contentPane:GetChild("btn_register")
    self.btn_login = self.contentPane:GetChild("btn_login")
    self.txt_account = self.contentPane:GetChild("txt_account")
    self.txt_password = self.contentPane:GetChild("txt_password")
    self.txt_password:GetChild("title").displayAsPassword = true
    self:Center()

end

function LoginWindow:OnShown()
end
function LoginWindow:OnHide()
end
function LoginWindow:OnRegisterEvent()
    local function toRegister()
        WindowManager.CloseWindow("LoginWindow")
        WindowManager.OpenWindow("RegisterWindow")
    end

    local function toLogin()
        local accountName = self.txt_account.title;
        local password = self.txt_password.title;
        local accountClass=AccountClass:New()
        accountClass.accountName=accountName
        accountClass.password=password
        LoginController:login(accountClass)
    end

    self.btn_register.onClick:Add(toRegister)
    self.btn_login.onClick:Add(toLogin)

    local OnLoginSuccess = function(accountClass)
        AccountModel:setCurAccountName(accountClass.accountName)
        WindowManager.CloseWindow("LoginWindow")
    end

    local OnLoginFail = function()
        WindowManager.CloseWindow("LoginWindow")
    end
    ModuleEvent.AddListenner(ModuleConst.LOGIN_SUCCESS, OnLoginSuccess)
    ModuleEvent.AddListenner(ModuleConst.LOGIN_Fail, OnLoginFail)
end
return LoginWindow