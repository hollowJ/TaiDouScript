--- @class RegisterWindow : BaseWindow
RegisterWindow = fgui.window_class(BaseWindow)

--构建函数
function RegisterWindow:ctor()
end

--可覆盖的函数(可选，不是说必须）
function RegisterWindow:OnInit()
    LogHelper.Info("HhhWindow:OnInit()")
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoginPackage", "win_register")
    self:Center()
    self.btn_cancel = self.contentPane:GetChild("btn_cancel")
    self.btn_register = self.contentPane:GetChild("btn_register")
    self.txt_account = self.contentPane:GetChild("txt_account")
    self.txt_password = self.contentPane:GetChild("txt_password")
    self.txt_rePassward = self.contentPane:GetChild("txt_repassword")
    self.txt_password:GetChild("title").displayAsPassword = true
    self.txt_rePassward:GetChild("title").displayAsPassword = true
end

function RegisterWindow:OnShown()
    LogHelper.Info("HhhWindow:OnShown()")
end
function RegisterWindow:OnHide()
end
local function OnRegisterSuccess(accountClass)
    LogHelper.Info("注册成功")
    ModuleEvent.Dispatch(ModuleConst.LOGIN_SUCCESS, accountClass)
    RegisterWindow:CloseWindow()
end
local function OnRegisterFail ()
    LogHelper.Info("注册失败")

end
local function register()
    local account = RegisterWindow.txt_account.title
    local password = RegisterWindow.txt_password.title
    local rePassword = RegisterWindow.txt_rePassward.title
    if (password ~= rePassword) then
        LogHelper.Info("两次密码不一致")
        return
    end
    local accountClass = AccountClass:New()
    accountClass.accountName = account
    accountClass.password = password
    LoginController:register(accountClass)
end
function RegisterWindow:OnRegisterEvent()

    self.btn_register.onClick:Add(register)
    ModuleEvent.AddListenner(ModuleConst.REGISTER_SUCCESS, OnRegisterSuccess)
    ModuleEvent.AddListenner(ModuleConst.REGISTER_FAIL, OnRegisterFail)
end
return RegisterWindow