---
--- Created by hollowJ.
--- DateTime: 2018/1/11 22:16


LoginController = {}
function LoginController:login(accountClass)
    local result = AccountModel:verify(accountClass)
    if result then
        ModuleEvent.Dispatch(ModuleConst.LOGIN_SUCCESS, accountClass)
    end
end
function LoginController:register(accountClass)
    local result = AccountModel:addAccount(accountClass)
    if result then
        ModuleEvent.Dispatch(ModuleConst.REGISTER_SUCCESS, accountClass)
    end
end
function LoginController:requireServers()
    ModuleEvent.Dispatch(ModuleConst.SERVERS_UPDATE)
    ModuleEvent.Dispatch(ModuleConst.SERVER_CHOOSE)
end
return LoginController