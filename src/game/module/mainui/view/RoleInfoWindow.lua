--- @class RoleInfoWindow : BaseWindow
RoleInfoWindow = fgui.window_class(BaseWindow)

local timer
local function update()
    local now = os.time()
    local hour = 0
    local minuter = 0
    local second = 0
    --剩余体力恢复秒数
    local leftHpSecond = 60 - (now - player.base:lastRecoverTime())
    if leftHpSecond > 0 then
        hour = math.floor(leftHpSecond / (60 * 60))
        minuter = math.floor(leftHpSecond / 60)
        second = leftHpSecond - hour * 60 * 60 - minuter * 60
    end
    local levelConfig = LevelModel.GetLevelConfigByLv(player.base:lv())
    RoleInfoWindow.txt_recoveryHp.text = hour .. ":" .. minuter .. ":" .. second
    RoleInfoWindow.txt_recoveryTrain.text = hour .. ":" .. minuter .. ":" .. second
    local otherHp = player.attr.hp - player.base:hp() - levelConfig.hpRecoverySpeed
    if otherHp > 0 then
        local otherHpSecond = math.ceil(otherHp / levelConfig.hpRecoverySpeed) * 60
        if otherHpSecond > 0 then
            local otherHpHour = math.floor(otherHpSecond / (60 * 60))
            local otherHpMinute = math.floor((otherHpSecond-otherHpHour * 60 * 60)/60)
            local otherHpSecond = otherHpSecond - otherHpHour * 60 * 60 - otherHpMinute * 60
            RoleInfoWindow.txt_recoveryAllHp.text = (hour + otherHpHour) .. ":" .. (minuter + otherHpMinute) .. ":" .. (second + otherHpSecond)
        end
    else
        RoleInfoWindow.txt_recoveryAllHp.text = hour .. ":" .. minuter .. ":" .. second
    end
    local otherTrain = levelConfig.maxTrainNum - player.base:trainNum() - levelConfig.trainRecoverySpeed
    if otherTrain > 0 then
        local otherTrainSecond = math.ceil(otherTrain / levelConfig.trainRecoverySpeed) * 60
        if otherTrainSecond > 0 then
            local otherTrainHour = math.floor(otherTrainSecond / (60 * 60))
            local otherTrainMinute = math.floor((otherTrainSecond -otherTrainHour*60*60)/ 60)
            local otherTrainSecond = otherTrainSecond - otherTrainHour * 60 * 60 - otherTrainMinute * 60
            RoleInfoWindow.txt_recoveryAllTrain.text = (hour + otherTrainHour) .. ":" .. (minuter + otherTrainMinute) .. ":" .. (second + otherTrainSecond)
        end
    else
        RoleInfoWindow.txt_recoveryAllTrain.text = hour .. ":" .. minuter .. ":" .. second
    end
end
--可覆盖的函数(可选，不是说必须）
function RoleInfoWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("MainUIPackage", "win_roleInfo")
    self.modal = true
    self.loader_headIcon = self.contentPane:GetChild("loader_headIcon")
    self.txt_name = self.contentPane:GetChild("txt_name")
    self.txt_vipLv = self.contentPane:GetChild("txt_vipLv")
    self.txt_LV = self.contentPane:GetChild("txt_LV")
    self.txt_fight = self.contentPane:GetChild("txt_fight")
    self.btn_changeName = self.contentPane:GetChild("btn_changeName")
    self.img_exp = self.contentPane:GetChild("img_exp")
    self.txt_exp = self.contentPane:GetChild("txt_exp")
    self.txt_crystalNum = self.contentPane:GetChild("txt_crystalNum")
    self.txt_goldNum = self.contentPane:GetChild("txt_goldNum")
    self.txt_hp = self.contentPane:GetChild("txt_Hp")
    self.txt_train = self.contentPane:GetChild("txt_train")
    self.txt_recoveryHp = self.contentPane:GetChild("txt_recoveryHp")
    self.txt_recoveryTrain = self.contentPane:GetChild("txt_recoveryTrain")
    self.txt_recoveryAllHp = self.contentPane:GetChild("txt_recoveryAllHp")
    self.txt_recoveryAllTrain = self.contentPane:GetChild("txt_recoveryAllTrain")
    timer = Timer.New(update, 1, -1, false)
    self:Center()
end

function RoleInfoWindow:OnShown()
    local job = player.base:job()
    local iconStr = JobsModel.GetJobById(job).icon
    local levelConfig = LevelModel.GetLevelConfigByLv(player.base:lv())
    local iconArr = StringUtil.split(iconStr, '+')
    RoleInfoWindow.txt_LV.text = player.base:lv()
    RoleInfoWindow.txt_fight.text = player.base:fight()
    RoleInfoWindow.loader_headIcon.url = ResourceManager.getResourceURL(iconArr[1], iconArr[2])
    RoleInfoWindow.txt_name.text = player.base:name()
    RoleInfoWindow.txt_hp.text = player.base:hp() .. "/" .. player.attr.hp
    RoleInfoWindow.txt_train.text = player.base:trainNum() .. "/" .. levelConfig.maxTrainNum
    RoleInfoWindow.txt_crystalNum.text = player.base:crystal()
    RoleInfoWindow.txt_goldNum.text = player.base:gold()
    RoleInfoWindow.txt_exp.text = player.base:exp() .. "/" .. levelConfig.nextLvExp
    RoleInfoWindow.img_exp.fillAmount = player.base:exp() / levelConfig.nextLvExp
    self.txt_vipLv.text = player.base:vipLv()
    update()
    timer:Start()
end

function RoleInfoWindow:OnHide()
    FrameTimerManager.RemoveUpdate(timer)
    timer:Stop()

end

function RoleInfoWindow:DoShowAnimation()
    self:OnShown()
end

function RoleInfoWindow:DoHideAnimation()
    self:HideImmediately()
end
local function actionEvent(context)
    local sender = context.sender
    if sender == RoleInfoWindow.btn_changeName then
        WindowManager.OpenWindow("ChangeNameWindow")
    end
end
local function onPropertyChange(propertyName, propertyNum)
    local levelConfig = LevelModel.GetLevelConfigByLv(player.base:lv())
    if propertyName == RoleProperty.hp then
        RoleInfoWindow.txt_hp.text = player.base:hp() .. "/" .. player.attr.hp
    elseif propertyName == RoleProperty.trainNum then
        RoleInfoWindow.txt_train.text = player.base:trainNum() .. "/" .. levelConfig.maxTrainNum
    elseif propertyName == RoleProperty.name then
        RoleInfoWindow.txt_name.text = player.base:name()
    end
end
function RoleInfoWindow:OnRegisterEvent()
    self.btn_changeName.onClick:Add(actionEvent)
    ModuleEvent.AddListenner(ModuleConst.CHANGE_STATUS, onPropertyChange)
end

return RoleInfoWindow