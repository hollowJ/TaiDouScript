--- @class MainUIWindow : BaseWindow
MainUIWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function MainUIWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("MainUIPackage", "win_main")
    self.loader_headIcon = self.contentPane:GetChild("com_headIcon"):GetChild("loader_headIcon")
    self.list_menu = self.contentPane:GetChild("list_menu")
    self.loader_headIcon = self.contentPane:GetChild("com_headIcon"):GetChild("loader_headIcon")
    self.txt_LV = self.contentPane:GetChild("com_headIcon"):GetChild("txt_LV")
    self.txt_name = self.contentPane:GetChild("com_headIcon"):GetChild("txt_name")
    self.img_hp = self.contentPane:GetChild("com_headIcon"):GetChild("img_hp")
    self.txt_hp = self.contentPane:GetChild("com_headIcon"):GetChild("txt_hp")
    self.btn_addHp = self.contentPane:GetChild("com_headIcon"):GetChild("btn_addHp")

    self.img_train = self.contentPane:GetChild("com_headIcon"):GetChild("img_train")
    self.txt_train = self.contentPane:GetChild("com_headIcon"):GetChild("txt_train")
    self.btn_addTrain = self.contentPane:GetChild("com_headIcon"):GetChild("btn_addTrain")

    self.txt_goldNum = self.contentPane:GetChild("com_gold"):GetChild("txt_Num")
    self.btn_addGold = self.contentPane:GetChild("com_gold"):GetChild("btn_addGold")

    self.txt_crystalNum = self.contentPane:GetChild("com_crystal"):GetChild("txt_Num")
    self.btn_addCrystal = self.contentPane:GetChild("com_crystal"):GetChild("btn_addCrystal")

end
local function updateHeadIcon()
    local levelConfig = LevelModel.GetLevelConfigByLv(player.base:lv())

    local job = player.base:job()
    local iconStr = JobsModel.GetJobById(job).icon
    local iconArr = StringUtil.split(iconStr, '+')
    MainUIWindow.txt_LV.text = player.base:lv()
    MainUIWindow.loader_headIcon.url = ResourceManager.getResourceURL(iconArr[1], iconArr[2])
    MainUIWindow.txt_name.text = player.base:name()
    MainUIWindow.txt_hp.text = player.base:hp() .. "/" .. player.attr.hp
    MainUIWindow.img_hp.fillAmount = player.base:hp() / player.attr.hp
    MainUIWindow.txt_train.text = player.base:trainNum() .. "/" .. levelConfig.maxTrainNum
    MainUIWindow.img_train.fillAmount = player.base:trainNum() / levelConfig.maxTrainNum
end
local function onMenuClick(context)
    local sender = context.sender
    local menuConfig = sender.data
    if menuConfig.id == 1 then
        --背包

        WindowManager.OpenWindow("BagWindow")
    elseif menuConfig.id == 2 then
        --任务

        TaskWindow:ShowWindow()

    elseif menuConfig.id == 3 then
        --技能

    elseif menuConfig.id == 4 then
        --战斗

    elseif menuConfig.id == 5 then
        --商城

    elseif menuConfig.id == 6 then
        --系统

    end
end
function MainUIWindow:OnShown()
    ---菜单
    self.list_menu.itemRenderer = function(index, item)
        local mainMenuConfig = MainMenusModel.GetMenuConfigByIndex(index + 1)
        item.title = mainMenuConfig.name
        item.icon = ResourceManager.getResourceURL("MainUIPackage", mainMenuConfig.icon)
        item.data = mainMenuConfig
        item.onClick:Add(onMenuClick)
    end
    self.list_menu.numItems = #MainMenusModel.GetAllMenuConfig()
    ---头像部分
    updateHeadIcon()
    ---金币钻石
    self.txt_goldNum.text = player.base:gold()
    self.txt_crystalNum.text = player.base:crystal()
end
function MainUIWindow:OnHide()
end
function MainUIWindow:DoShowAnimation()
    self:OnShown()
end
function MainUIWindow:DoHideAnimation()
    self:HideImmediately()
end

local function loadStageSuccess(stageId)
    if (stageId == 2) then
        WindowManager.OpenWindow("MainUIWindow")
    end
end

function MainUIWindow:ActionEvent(context)
    local sender = context.sender
    if sender == MainUIWindow.loader_headIcon then
        WindowManager.OpenWindow("RoleInfoWindow")

    elseif sender == MainUIWindow.btn_addHp then
    elseif sender == MainUIWindow.btn_addTrain then
    elseif sender == MainUIWindow.btn_addGold then
    elseif sender == MainUIWindow.btn_addCrystal then
    end
end
local function refreshStatus()
    local levelConfig = LevelModel.GetLevelConfigByLv(player.base:lv())
    MainUIWindow.txt_hp.text = player.base:hp() .. "/" .. player.attr.hp
    MainUIWindow.img_hp.fillAmount = player.base:hp() / player.attr.hp
    MainUIWindow.txt_train.text = player.base:trainNum() .. "/" .. levelConfig.maxTrainNum
    MainUIWindow.img_train.fillAmount = player.base:trainNum() / levelConfig.maxTrainNum
    MainUIWindow.txt_name.text = player.base:name()
end
local function onStatusChange(propertyName, propertyNum)
    if propertyName == RoleProperty.hp or propertyName == RoleProperty.trainNum or propertyName == RoleProperty.name then
        refreshStatus()
    end
end
local function onItemChange(itemNo, count)
    if itemNo == ItemConst.GOLD then
        MainUIWindow.txt_goldNum.text = player.base:gold()
    elseif itemNo == ItemConst.CRYSTAL then
        MainUIWindow.txt_crystalNum.text = player.base:crystal()
    end
end
local function onLevelUp(lv)
    MainUIWindow.txt_LV.text = player.base:lv()
end
local function onChangeName(newName)
    MainUIWindow.txt_name.text = player.base:name()
end

function MainUIWindow:OnRegisterEvent()
    self:AddClickListenner(self.loader_headIcon)
    self:AddClickListenner(self.btn_addHp)
    self:AddClickListenner(self.btn_addTrain)
    self:AddClickListenner(self.btn_addGold)
    self:AddClickListenner(self.btn_addCrystal)
    ModuleEvent.AddListenner(ModuleConst.LEVEL_UP, onLevelUp)
    ModuleEvent.AddListenner(ModuleConst.CHANGE_STATUS, onStatusChange)
    ModuleEvent.AddListenner(ModuleConst.BAG_ITEM_CHANGE, onItemChange)
    ModuleEvent.AddListenner(ModuleConst.CHANGE_ATTR, refreshStatus)
    ModuleEvent.AddListenner(ModuleConst.CHANGE_NAME, onChangeName)
end
function MainUIWindow.OnFirstImport()
    ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_SUCCESS, loadStageSuccess)
end
return MainUIWindow