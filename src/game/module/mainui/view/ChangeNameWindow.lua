--- @class ChangeNameWindow : BaseWindow
ChangeNameWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function ChangeNameWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("MainUIPackage", "win_changeName")
    self.modal = true
    self.btn_ok = self.contentPane:GetChild("btn_ok")
    self.btn_cancel = self.contentPane:GetChild("btn_cancel")
    self.txt_name = self.contentPane:GetChild("txt_name")
    self:Center()
end

function ChangeNameWindow:OnShown()

end

function ChangeNameWindow:OnHide()

end
local function changeName(newName)
    if StringUtil.trim(newName) ~= "" then
        player.base:ChangeName(newName)
        ChangeNameWindow:CloseWindow()
    end
end
function ChangeNameWindow:ActionEvent(context)
    local sender = context.sender
    if sender == self.btn_ok then
        local newName = ChangeNameWindow.txt_name.title
        changeName(newName)

    elseif sender == self.btn_cancel then
        self:CloseWindow()
    end
end
function ChangeNameWindow:OnRegisterEvent()
    self:AddClickListenner(self.btn_ok)
    self:AddClickListenner(self.btn_cancel)

end

return ChangeNameWindow