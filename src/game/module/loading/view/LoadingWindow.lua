--- @class LoadingWindow : BaseWindow
LoadingWindow = fgui.window_class(BaseWindow)
local curStage = nil
--可覆盖的函数(可选，不是说必须）
function LoadingWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("LoadingPackage", "win_loading")
    self.loader_loadingBG = self.contentPane:GetChild("loader_loadingBG")
    ---@type FairyGUI.GProgressBar
    self.progress_loading = self.contentPane:GetChild("progress_loading")
end

function LoadingWindow:OnShown()
    local picPath = StageModel.GetStageById(curStage).loadingBG
    local pathArr = StringUtil.split(picPath, "+")
    self.loader_loadingBG.url = ResourceManager.getResourceURL(pathArr[1], pathArr[2])
    self.progress_loading:Update(0)
end
local function onLoadStageStart(stageId)
    curStage = stageId
    WindowManager.OpenWindow("LoadingWindow")
end
local function onLoadStageSuccess(stageId)
    LoadingWindow:CloseWindow()
end
function LoadingWindow:OnRegisterEvent()
    ModuleEvent.AddListenner(ModuleConst.LOADING_PROGRESS_UPDATE, FuncWrap(self.progress_loading.Update,self.progress_loading))
end
function LoadingWindow:Update()

end
ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_SUCCESS, onLoadStageSuccess)
ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_START, onLoadStageStart)
return LoadingWindow