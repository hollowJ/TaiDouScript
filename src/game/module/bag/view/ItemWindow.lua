---@class ItemWindow:BaseWindow
ItemWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function ItemWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("BagPackage", "win_item")
    self.modal = true
    self.txt_name = self.contentPane:GetChild("txt_name")
    self.txt_typeName = self.contentPane:GetChild("txt_typeName")
    self.txt_desc = self.contentPane:GetChild("txt_desc")
    self.com_Item = self.contentPane:GetChild("com_Item")
    self.btn_use = self.contentPane:GetChild("btn_use")
    self.btn_useBatch = self.contentPane:GetChild("btn_useBatch")

    self.dragArea = nil

    self:fixPosition()
end
function ItemWindow:fixPosition()
    if WindowManager.IsShow(BagWindow.windowName) then
        self:SetXY((BagWindow.x + BagWindow.width) / 2, (BagWindow.y + BagWindow.height) / 3)
    else
        self:Center()
    end
end
function ItemWindow:DoShowAnimation()
    self:OnShown()
end
function ItemWindow:DoHideAnimation()
    self:HideImmediately()
end
function ItemWindow:updateData()
    local toolId = ItemModel.curToolId
    ---@type Tool
    local tool = player.bagContainer:getToolByToolId(toolId)
    --- @type ItemClass
    local item = ItemModel.getItemByItemNo(tool.itemNo)
    self.txt_name.text = item.name
    self.txt_typeName.text = ItemType[item.type]
    self.txt_desc.text = item.desc
    self.com_Item.icon = ResourceManager.getResourceURL(item.icon)
    self.com_Item.title = tool.count
end
function ItemWindow:OnShown()
    self:fixPosition()
    self:updateData()
end
function ItemWindow:OnHide()
end
function ItemWindow:ActionEvent(context)
    local sender = context.sender
    if string.find(sender.name, "equip_") then
        if sender == ItemWindow.equip_2 then
            LogHelper.Debug("hwj")
        end
    elseif sender.name == "btn_use" then
        local toolId = ItemModel.curToolId
        local tool = player.bagContainer:getToolByToolId(toolId)
        if tool:Use() then
            self:CloseWindow()
        end
    elseif sender.name == "btn_useBatch" then
        local toolId = ItemModel.curToolId
        local tool = player.bagContainer:getToolByToolId(toolId)
        if tool:Use() then
            self:CloseWindow()
         end
    end
end

function ItemWindow:OnRegisterEvent()
    self:AddClickListenner(self.btn_use)
    self:AddClickListenner(self.btn_useBatch)
end

return ItemWindow