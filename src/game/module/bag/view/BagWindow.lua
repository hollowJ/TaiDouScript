---@class BagWindow:BaseWindow
BagWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function BagWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("BagPackage", "win_bag")
    self.modal = true
    self.equip_1 = self.contentPane:GetChild("equip_1")
    self.equip_2 = self.contentPane:GetChild("equip_2")
    self.equip_3 = self.contentPane:GetChild("equip_3")
    self.equip_4 = self.contentPane:GetChild("equip_4")
    self.equip_5 = self.contentPane:GetChild("equip_5")
    self.equip_6 = self.contentPane:GetChild("equip_6")
    self.equip_7 = self.contentPane:GetChild("equip_7")
    self.equip_8 = self.contentPane:GetChild("equip_8")
    ---@type FairyGUI.GList
    self.com_bagList = self.contentPane:GetChild("com_bagList"):GetChild("list_bag")

    self.model_holder = self.contentPane:GetChild("model_holder")
    self.btn_desc = self.contentPane:GetChild("btn_desc")
    self.txt_ownNum = self.contentPane:GetChild("txt_ownNum")
    self.txt_hp = self.contentPane:GetChild("txt_hp")
    self.txt_damage = self.contentPane:GetChild("txt_damage")
    self.txt_ownNum = self.contentPane:GetChild("txt_ownNum")
    self.txt_exp = self.contentPane:GetChild("txt_exp")
    self.img_exp = self.contentPane:GetChild("img_exp")
    self.btn_sell = self.contentPane:GetChild("btn_sell")
    self.graph_holder = self.contentPane:GetChild("model_holder"):GetChild("graph_holder")
    self.goWrapper = GoWrapper.New()
    self.graph_holder:SetNativeObject(self.goWrapper)
    self:Center()
end
local function OnClickItem(context)
    local sender = context.sender
    if sender.data then
        BagController.ShowItem(sender.data.id, 1)
    end
end
function BagWindow:ShowModel()
    CharacterController:SetUpCharacterModel(player.base:job(), BagWindow.goWrapper)
end
function BagWindow:HideModel()
    CharacterController:RecycleCharacterModel(BagWindow.goWrapper)

end
function BagWindow:OnShown()
    self:updateData()
end
function BagWindow:updateData()
    if not self:IsShow() then
        return
    end
    ModuleEvent.Dispatch(ModuleConst.BAG_WINDOW_SHOW)
    self:ShowModel()
    local tools = player.bagContainer:getTools()
    local tool_index = 1
    local showCount=0
    self.com_bagList.itemRenderer = function(index, itemCom)
        local tool = nil
        local icon = nil
        local title = nil
        if tool_index <= #tools then
            for _toolIndex = tool_index, #tools do
                ---@type ToolClass
                local theTool = tools[_toolIndex]
                local show=true;
                if theTool.isWearing ==1 then
                    show=false
                end
                if theTool:GetItem():IsVirtual() then
                    show=false
                end
                if show then
                    local item = ItemModel.getItemByItemNo(theTool.itemNo)
                    icon = ResourceManager.getResourceURL(item.icon)
                    title = theTool.count
                    tool = theTool
                    tool_index = _toolIndex + 1
                    showCount=showCount+1
                    break
                end
            end

        end
        itemCom.icon = icon
        itemCom.title = title
        itemCom.data = tool
        itemCom.onClick:Add(OnClickItem)
    end
    self.com_bagList.numItems = player.bagContainer.bagNum
    self.txt_ownNum.text = showCount.. "/" .. player.bagContainer.bagNum
    for position, positionName in ipairs(EquipType) do
        local tool = player.bagContainer:getEquipByPosition(position)
        ---@type Tool
        if tool then
            local comEquip = self["equip_" .. position]
            local item = ItemModel.getItemByItemNo(tool.itemNo)
            comEquip.icon = ResourceManager.getResourceURL(item.icon)
            comEquip.data = tool.id
        else
            local comEquip = self["equip_" .. position]
            comEquip.icon = ""
            comEquip.data = nil
        end

    end
    self.txt_hp .text = player.attr.hp
    self.txt_damage = player.attr.attack
    local levelConfig = player.base:getCurLevelConfig()
    self.txt_exp.text = player.base:exp() .. "/" .. levelConfig.nextLvExp
    self.img_exp.fillAmount = player.base:exp() / levelConfig.nextLvExp
end
function BagWindow:OnHide()
    self:HideModel()
    ModuleEvent.Dispatch(ModuleConst.BAG_WINDOW_HIDE)
end
function BagWindow:ActionEvent(context)
    local sender = context.sender
    if string.find(sender.name, "equip_") then
        BagController.ShowItem(sender.data, 2)
    elseif sender.name == "btn_sell" then
        ---@type ToolClass
        local tool = self.com_bagList:GetChildAt(self.com_bagList.selectedIndex).data
        if tool then
            BagController.SellTool(tool)
            EquipWindow:CloseWindow()
        end
    end
end
function BagWindow:OnRegisterEvent()
    self:AddClickListenner(self.equip_1)
    self:AddClickListenner(self.equip_2)
    self:AddClickListenner(self.equip_3)
    self:AddClickListenner(self.equip_4)
    self:AddClickListenner(self.equip_5)
    self:AddClickListenner(self.equip_6)
    self:AddClickListenner(self.equip_7)
    self:AddClickListenner(self.equip_8)
    self:AddClickListenner(self.btn_desc)
    self:AddClickListenner(self.btn_sell)
    ModuleEvent.AddListenner(ModuleConst.EQUIP_WINDOW_SHOW, FuncWrap(BagWindow.HideModel, BagWindow))
    ModuleEvent.AddListenner(ModuleConst.EQUIP_WINDOW_HIDE, FuncWrap(BagWindow.ShowModel, BagWindow))
    ModuleEvent.AddListenner(ModuleConst.EQUIP_TAKE_OFF_SUCCESS, FuncWrap(BagWindow.updateData, BagWindow))
    ModuleEvent.AddListenner(ModuleConst.EQUIP_TAKE_ON_SUCCESS, FuncWrap(BagWindow.updateData, BagWindow))
    ModuleEvent.AddListenner(ModuleConst.BAG_REFRESH, FuncWrap(BagWindow.updateData, BagWindow))
    ModuleEvent.AddListenner(ModuleConst.CHANGE_ATTR, FuncWrap(BagWindow.updateData, BagWindow))


end

return BagWindow