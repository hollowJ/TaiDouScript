--- @class TaskWindow : BaseWindow
TaskWindow = fgui.window_class(BaseWindow)
local tabMenus = { { type = 1, name = "主线任务", img = "pic_主线" }, { type = 2, name = "日常任务", img = "pic_日常" }, { type = 3, name = "赏金任务", img = "pic_奖赏" } }
--可覆盖的函数(可选，不是说必须）
function TaskWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("TaskPackage", "win_task")
    self.modal = true
    ---@type FairyGUI.GList
    self.list_tasks = self.contentPane:GetChild("list_tasks")
    ---@type FairyGUI.GList
    self.list_tab = self.contentPane:GetChild("list_tab")
    self.tasks = nil
    self:Center()
    self.list_tasks.itemRenderer = function(index, taskItem)
        ---@type FairyGUI.GComponent
        local img_taskType = taskItem:GetChild("img_taskType")
        local img_reward = taskItem:GetChild("img_reward")
        local txt_taskName = taskItem:GetChild("txt_taskName")
        local txt_taskDesc = taskItem:GetChild("txt_taskDesc")
        local txt_gold = taskItem:GetChild("txt_gold")
        local txt_crystal = taskItem:GetChild("txt_crystal")
        ---@type FairyGUI.GButton
        local btn_takeReward = taskItem:GetChild("btn_takeReward")
        ---@type FairyGUI.GButton
        local btn_getTask = taskItem:GetChild("btn_getTask")
        ---@type FairyGUI.GButton
        local btn_goto = taskItem:GetChild("btn_goto")
        local task = self.tasks[index + 1]
        local taskType = tabMenus[task.type]
        img_taskType.url = ResourceManager.getResourceURL("TaskPackage", taskType.img)
        img_reward.icon = ResourceManager.getResourceURLByItemNo(task.equipItemNo)
        txt_taskName.text = task.taskName
        txt_taskDesc.text = task.taskDesc
        txt_gold.text = " * " .. task.goldNum
        txt_crystal.text = " * " .. task.crystalNum
        btn_getTask.visible = false
        btn_goto.visible = false
        btn_takeReward.visible = false
        local btn_func = nil
        local myTask = player.taskContainer:GetTask(task.id)
        local state = TaskDefine.NOT_GET
        if myTask then
            state = myTask.state
            txt_taskName.text = task.taskName .. myTask:Progress()

        end
        if state == TaskDefine.NOT_GET then
            btn_func = btn_getTask
        elseif state == TaskDefine.PROGRESSING then
            btn_func = btn_goto
        elseif state == TaskDefine.COMPLETE then
            btn_func = btn_takeReward
        end
        btn_func.visible = true
        btn_func.data = task.id
        self:AddClickListenner(btn_func)
    end
    self.list_tab.itemRenderer = function(index, tabItem)
        local taskType = tabMenus[index + 1]
        tabItem.text = taskType.name
        tabItem.data = taskType.type
    end
end
local function onTabClick(window, context)
    local taskType = context.data.data
    window:UpdateTaskByType(taskType)
end
function TaskWindow:OnShown()
    self.list_tab.numItems = #tabMenus
    self.list_tab.onClickItem:Add(FuncWrap(onTabClick, self))
    self.list_tab:GetChildAt(0).onClick:Call()
end
function TaskWindow:UpdateTaskByType(type)
    self.list_tasks:RemoveChildrenToPool()
    self.tasks = TaskModel.GetTasksByType(type)
    self.list_tasks.numItems = #self.tasks
end
function TaskWindow:ActionEvent(context)
    local sender = context.sender
    if string.find(sender.name, "btn_getTask") then
        local taskId = sender.data
        TaskController.AddTask(taskId)

    elseif string.find(sender.name, "btn_goto") then
        local taskId = sender.data
        local task = TaskModel.GetTaskById(taskId)
    elseif string.find(sender.name, "btn_takeReward") then
        local taskId = sender.data
        local myTask = player.taskContainer:GetTask(taskId)
        BagController.AddTool(myTask.reward)
        player.taskContainer:RemoveTask(myTask.id)
        self:UpdateTaskByType(myTask.type)
    end
end
function TaskWindow:OnRegisterEvent()
end

function TaskWindow:OnHotUpdate()
end

return TaskWindow