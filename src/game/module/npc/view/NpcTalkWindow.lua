--- @class NpcTalkWindow : BaseWindow
NpcTalkWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function NpcTalkWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("NpcPackage", "win_TalkPanel")
    self:Center()
    self.y = self.parent.height - self.height - 10
    self.modal = true
    ---@type FairyGUI.GLoader
    self.img_npcIcon = self.contentPane:GetChild("img_npcIcon")
    ---@type FairyGUI.GRichTextField
    self.txt_talkContent = self.contentPane:GetChild("txt_talkContent")
    ---@type FairyGUI.GTextField
    self.txt_npcName = self.contentPane:GetChild("txt_npcName")
    ---@type FairyGUI.GButton
    self.btn_bgImg = self.contentPane:GetChild("btn_bgImg")
end
function NpcTalkWindow:OnShown()

end
function NpcTalkWindow:ActionEvent(context)
    local sender = context.sender
    if string.find(sender.name, "btn_bgImg") then
        self:CloseWindow()
    elseif string.find(sender.name, "btn_goto") then
    end
end
function NpcTalkWindow:OnRegisterEvent()
    self:AddClickListenner(self.btn_bgImg)
end

function NpcTalkWindow:OnHotUpdate()
end

return NpcTalkWindow