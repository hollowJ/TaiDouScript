---@class FightScoreWindow:BaseWindow
FightScoreWindow = fgui.window_class(BaseWindow)
local tweener = nil

--可覆盖的函数(可选，不是说必须）
function FightScoreWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("ChatPackage", "win_fight_score")
    self.modal = false
    self.focusable = false
    self.sortingOrder = 10
    self.txt_fightScore = self.contentPane:GetChild("txt_fightScore")
    self.txt_fightScore.alpha = 0
    self:Center()
    self.y = GRoot.inst.height * 2 / 3
end

function FightScoreWindow:Update()
end

function FightScoreWindow:ActionEvent(context)
    local sender = context.sender

end

local function onChangeFightScore(oldFightScore, newFightScore, notify)
    if notify then
        FightScoreWindow:ChangeFightScore(oldFightScore, newFightScore)
    end
end
function FightScoreWindow:ChangeFightScore(oldFightScore, newFightScore)
    if self:IsShow() then
        FightScoreWindow.txt_fightScore.alpha = 1
        if tweener and tweener:IsPlaying() then
            --tweener:Complete(true)
            tweener:Kill()
            LogHelper.Info("self.tweener:Kill(true)")
        end
        tweener = TweenUtils.TweenInt(oldFightScore, newFightScore, 1.5, function(fightScore)
            FightScoreWindow.txt_fightScore.text = fightScore
        end)
        TweenUtils.OnComplete(tweener, function()
            tweener = FightScoreWindow.txt_fightScore:TweenFade(0, 4)
        end)
    end

end
function FightScoreWindow:OnRegisterEvent()
    ModuleEvent.AddListenner(ModuleConst.CHANGE_FIGHT_SCORE, onChangeFightScore)

end
local function loadStageSuccess(stageId)
    FightScoreWindow:ShowWindow()
end
ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_SUCCESS, loadStageSuccess)

return FightScoreWindow