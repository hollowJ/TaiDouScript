---@class NotifyWindow:BaseWindow
NotifyWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function NotifyWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("ChatPackage", "win_notify")
    self.modal = false
    self.focusable = false
    self.sortingOrder = 10
    self.list_notify = self.contentPane:GetChild("list_notify")
    self:Center()
end

function NotifyWindow:Update()
    if #NotifyModel.notifyQueue > 0 then
        if NotifyWindow.list_notify.numChildren < 5 then
            local msg = NotifyModel.notifyQueue[1]
            table.remove(NotifyModel.notifyQueue, 1)
            NotifyWindow:notify(msg)
        end
    end
end
function NotifyWindow:notify(msg)
    local noticeMsgExtend = self.list_notify:AddItemFromPool()
    noticeMsgExtend.com_title.text = msg
    noticeMsgExtend.alpha = 1
    self.list_notify:EnsureBoundsCorrect()
    noticeMsgExtend:doHide(1)
end

function NotifyWindow:OnShown()

end

function NotifyWindow:OnHide()

end
function NotifyWindow:ActionEvent(context)
    local sender = context.sender

end
function NotifyWindow:OnRegisterEvent()
end
local function loadStageSuccess(stageId)
    WindowManager.OpenWindow("NotifyWindow")
end
ModuleEvent.AddListenner(ModuleConst.LOAD_STAGE_SUCCESS, loadStageSuccess)

return NotifyWindow