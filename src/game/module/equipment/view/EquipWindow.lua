--- @class EquipWindow : BaseWindow
EquipWindow = fgui.window_class(BaseWindow)
--可覆盖的函数(可选，不是说必须）
function EquipWindow:OnInit()
    UIPackage.AddPackage(self.assetBundle, false)
    self.contentPane = UIPackage.CreateObject("EquipPackage", "win_equip")
    --self.modal = true
    self.txt_equipName = self.contentPane:GetChild("txt_equipName")
    self.txt_quality = self.contentPane:GetChild("txt_quality")
    self.txt_typeName = self.contentPane:GetChild("txt_typeName")
    ---- @type FairyGUI.GList
    self.list_star = self.contentPane:GetChild("list_star")
    ---- @type FairyGUI.GList
    self.list_attr = self.contentPane:GetChild("list_attr")
    self.txt_effect = self.contentPane:GetChild("txt_effect")
    self.txt_desc = self.contentPane:GetChild("txt_desc")
    self.com_equip = self.contentPane:GetChild("com_equip")
    self.txt_fight = self.contentPane:GetChild("txt_fight")
    self.btn_use = self.contentPane:GetChild("btn_use")
    self.btn_upgrade = self.contentPane:GetChild("btn_upgrade")

    self.dragArea = nil
    self:fixPosition()
end
function EquipWindow:DoShowAnimation()
    self:OnShown()
end
function EquipWindow:DoHideAnimation()
    self:HideImmediately()
end
function EquipWindow:fixPosition()
    if WindowManager.IsShow(BagWindow.windowName) then
        self:SetXY(BagWindow.x, BagWindow.y)
        CharacterController:RecycleCharacterModel(BagWindow.goWrapper)
    else
        self:Center()
    end
end
function EquipWindow:OnShown()
    ModuleEvent.Dispatch(ModuleConst.EQUIP_WINDOW_SHOW)
    self:fixPosition()
    self:updateData()
end
function EquipWindow:updateData()
    ModuleEvent.Dispatch(ModuleConst.EQUIP_WINDOW_SHOW)
    self:fixPosition()
    local toolId = ItemModel.curToolId
    ---@type EquipClass
    local equip=player.bagContainer:getToolByToolId(toolId)
    local item=ItemModel.getItemByItemNo(equip.itemNo)
    self.txt_equipName.text = equip.name
    self.list_star.numItems = equip.starNum
    self.txt_quality.text = equip.starNum
    self.txt_typeName.text = EquipType[equip.type]
    self.txt_desc.text = item.desc
    self.txt_effect.text = ""
    self.com_equip.icon = ResourceManager.getResourceURL(item.icon)
    self.txt_fight.text = equip:getFightScore()
    local showAttrList1=equip:getShowAttrList1()
    local showAttrList2=equip:getShowAttrList2()
    ---@param item FairyGUI.GComponent
    self.list_attr.itemRenderer = function(index, item)
        local txt_attrName = item:GetChild("txt_attrName")
        local txt_attrValue = item:GetChild("txt_attrValue")
        local attr
        local color
        index=index+1
        if index<=#showAttrList1 then
            attr=showAttrList1[index]
            color="#FFFFFF"
        else
            attr=showAttrList2[index-#showAttrList1]
            color="#FF0000"
        end
        txt_attrName.text = string.format("[color=%s]%s[/color]",color,attr.attrName)
        txt_attrValue.text =string.format("[color=%s]%s[/color]",color,attr.attrValue)
    end
    self.list_attr.numItems = #showAttrList1+#showAttrList2
    if EquipModel.showEntry == 1 then
        self.btn_use.text = "装备"
    elseif EquipModel.showEntry == 2 then
        self.btn_use.text = "脱下"
    end
    self.btn_use.data = EquipModel.showEntry
end
function EquipWindow:OnHide()
    ModuleEvent.Dispatch(ModuleConst.EQUIP_WINDOW_HIDE)
end

function EquipWindow:ActionEvent(context)
    local sender = context.sender
    if sender == self.btn_use then
        if self.btn_use.data == 1 then
            local toolId = ItemModel.curToolId
            EquipController:takeOnEquip(toolId)
        else
            local toolId = ItemModel.curToolId
            EquipController:takeOffEquip(toolId)
        end

    elseif sender == self.btn_upgrade then
    end
end
function EquipWindow:OnRegisterEvent()
    ModuleEvent.AddListenner(ModuleConst.BAG_WINDOW_HIDE, FuncWrap(EquipWindow.CloseWindow, EquipWindow))
    ModuleEvent.AddListenner(ModuleConst.EQUIP_TAKE_ON_SUCCESS, FuncWrap(EquipWindow.CloseWindow, EquipWindow))
    ModuleEvent.AddListenner(ModuleConst.EQUIP_TAKE_OFF_SUCCESS, FuncWrap(EquipWindow.CloseWindow, EquipWindow))
    self:AddClickListenner(self.btn_use)
    self:AddClickListenner(self.btn_upgrade)
end

return EquipWindow